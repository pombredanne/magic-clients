package com.cythereal.magic.api;

import io.swagger.client.ApiException;
import java.io.File;
import com.cythereal.magic.model.QueryResponse;
import com.cythereal.magic.model.ResponseObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for FileOperationsApi
 */
public class FileOperationsApiTest {

    private final FileOperationsApi api = new FileOperationsApi();

    
    /**
     * Download a file
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void downloadTest() throws ApiException {
        String apiKey = null;
        String fileHash = null;
        // File response = api.download(apiKey, fileHash);

        // TODO: test validations
    }
    
    /**
     * Query file info and analysis status
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void queryTest() throws ApiException {
        String apiKey = null;
        String fileHash = null;
        // QueryResponse response = api.query(apiKey, fileHash);

        // TODO: test validations
    }
    
    /**
     * Reprocess a previously uploaded file
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void reprocessTest() throws ApiException {
        String apiKey = null;
        String fileHash = null;
        Integer priority = null;
        // ResponseObject response = api.reprocess(apiKey, fileHash, priority);

        // TODO: test validations
    }
    
    /**
     * Upload file for processing
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void uploadTest() throws ApiException {
        String apiKey = null;
        File filedata = null;
        String origFilePath = null;
        String password = null;
        Integer priority = null;
        Boolean force = null;
        // ResponseObject response = api.upload(apiKey, filedata, origFilePath, password, priority, force);

        // TODO: test validations
    }
    
}
