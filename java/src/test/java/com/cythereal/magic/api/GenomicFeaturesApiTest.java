package com.cythereal.magic.api;

import io.swagger.client.ApiException;
import com.cythereal.magic.model.BinaryFeaturesResponse;
import com.cythereal.magic.model.ProcedureFeaturesResponse;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for GenomicFeaturesApi
 */
public class GenomicFeaturesApiTest {

    private final GenomicFeaturesApi api = new GenomicFeaturesApi();

    
    /**
     * Show the genomic features for a given binary.
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void binaryFeaturesTest() throws ApiException {
        String apiKey = null;
        String fileHash = null;
        // BinaryFeaturesResponse response = api.binaryFeatures(apiKey, fileHash);

        // TODO: test validations
    }
    
    /**
     * Show the genomic features for a given procedure.
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void procedureFeaturesTest() throws ApiException {
        String apiKey = null;
        String fileHash = null;
        String procRva = null;
        // ProcedureFeaturesResponse response = api.procedureFeatures(apiKey, fileHash, procRva);

        // TODO: test validations
    }
    
}
