package com.cythereal.magic.api;

import io.swagger.client.ApiException;
import com.cythereal.magic.model.BinarySimilarityResponse;
import com.cythereal.magic.model.ProcedureSimilarityResponse;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for SimilarityApi
 */
public class SimilarityApiTest {

    private final SimilarityApi api = new SimilarityApi();

    
    /**
     * Search for similar binaries.
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void similarBinariesTest() throws ApiException {
        String apiKey = null;
        String fileHash = null;
        Float threshold = null;
        Integer level = null;
        // BinarySimilarityResponse response = api.similarBinaries(apiKey, fileHash, threshold, level);

        // TODO: test validations
    }
    
    /**
     * Search for procedures similar to a given procedure.
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void similarProceduresTest() throws ApiException {
        String apiKey = null;
        String fileHash = null;
        String procRva = null;
        // ProcedureSimilarityResponse response = api.similarProcedures(apiKey, fileHash, procRva);

        // TODO: test validations
    }
    
}
