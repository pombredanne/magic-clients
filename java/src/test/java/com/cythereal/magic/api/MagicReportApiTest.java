package com.cythereal.magic.api;

import io.swagger.client.ApiException;
import com.cythereal.magic.model.MagicCategoriesResponse;
import com.cythereal.magic.model.MagicMatchesResponse;
import com.cythereal.magic.model.ResponseObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for MagicReportApi
 */
public class MagicReportApiTest {

    private final MagicReportApi api = new MagicReportApi();

    
    /**
     * Retrieve the MAGIC categories for a binary. **Alpha Level Feature**
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void magicCategoriesTest() throws ApiException {
        String apiKey = null;
        String fileHash = null;
        // MagicCategoriesResponse response = api.magicCategories(apiKey, fileHash);

        // TODO: test validations
    }
    
    /**
     * Retrieve the MAGIC correlations for a binary.
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void magicCorrelationsTest() throws ApiException {
        String apiKey = null;
        String fileHash = null;
        Boolean details = null;
        Float threshold = null;
        // MagicMatchesResponse response = api.magicCorrelations(apiKey, fileHash, details, threshold);

        // TODO: test validations
    }
    
    /**
     * Save category for a binary. **Alpha Level Feature**
     *
     * 
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void putMagicCategoriesTest() throws ApiException {
        String apiKey = null;
        String fileHash = null;
        String category = null;
        // ResponseObject response = api.putMagicCategories(apiKey, fileHash, category);

        // TODO: test validations
    }
    
}
