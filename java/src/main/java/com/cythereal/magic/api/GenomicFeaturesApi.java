

package com.cythereal.magic.api;

import io.swagger.client.ApiCallback;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.ApiResponse;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;
import io.swagger.client.ProgressRequestBody;
import io.swagger.client.ProgressResponseBody;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;

import com.cythereal.magic.model.BinaryFeaturesResponse;
import com.cythereal.magic.model.ProcedureFeaturesResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GenomicFeaturesApi {
    private ApiClient apiClient;

    public GenomicFeaturesApi() {
        this(Configuration.getDefaultApiClient());
    }

    public GenomicFeaturesApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /* Build call for binaryFeatures */
    private com.squareup.okhttp.Call binaryFeaturesCall(String apiKey, String fileHash, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // verify the required parameter 'apiKey' is set
        if (apiKey == null) {
            throw new ApiException("Missing the required parameter 'apiKey' when calling binaryFeatures(Async)");
        }
        
        // verify the required parameter 'fileHash' is set
        if (fileHash == null) {
            throw new ApiException("Missing the required parameter 'fileHash' when calling binaryFeatures(Async)");
        }
        

        // create path and map variables
        String localVarPath = "/show/binary/{api_key}/{file_hash}".replaceAll("\\{format\\}","json")
        .replaceAll("\\{" + "api_key" + "\\}", apiClient.escapeString(apiKey.toString()))
        .replaceAll("\\{" + "file_hash" + "\\}", apiClient.escapeString(fileHash.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Show the genomic features for a given binary.
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @return BinaryFeaturesResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public BinaryFeaturesResponse binaryFeatures(String apiKey, String fileHash) throws ApiException {
        ApiResponse<BinaryFeaturesResponse> resp = binaryFeaturesWithHttpInfo(apiKey, fileHash);
        return resp.getData();
    }

    /**
     * Show the genomic features for a given binary.
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @return ApiResponse&lt;BinaryFeaturesResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<BinaryFeaturesResponse> binaryFeaturesWithHttpInfo(String apiKey, String fileHash) throws ApiException {
        com.squareup.okhttp.Call call = binaryFeaturesCall(apiKey, fileHash, null, null);
        Type localVarReturnType = new TypeToken<BinaryFeaturesResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Show the genomic features for a given binary. (asynchronously)
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call binaryFeaturesAsync(String apiKey, String fileHash, final ApiCallback<BinaryFeaturesResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = binaryFeaturesCall(apiKey, fileHash, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<BinaryFeaturesResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /* Build call for procedureFeatures */
    private com.squareup.okhttp.Call procedureFeaturesCall(String apiKey, String fileHash, String procRva, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // verify the required parameter 'apiKey' is set
        if (apiKey == null) {
            throw new ApiException("Missing the required parameter 'apiKey' when calling procedureFeatures(Async)");
        }
        
        // verify the required parameter 'fileHash' is set
        if (fileHash == null) {
            throw new ApiException("Missing the required parameter 'fileHash' when calling procedureFeatures(Async)");
        }
        
        // verify the required parameter 'procRva' is set
        if (procRva == null) {
            throw new ApiException("Missing the required parameter 'procRva' when calling procedureFeatures(Async)");
        }
        

        // create path and map variables
        String localVarPath = "/show/proc/{api_key}/{file_hash}/{proc_rva}".replaceAll("\\{format\\}","json")
        .replaceAll("\\{" + "api_key" + "\\}", apiClient.escapeString(apiKey.toString()))
        .replaceAll("\\{" + "file_hash" + "\\}", apiClient.escapeString(fileHash.toString()))
        .replaceAll("\\{" + "proc_rva" + "\\}", apiClient.escapeString(procRva.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Show the genomic features for a given procedure.
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param procRva The RVA of the procedure to find similarities with. Formatted 0xnnnn (required)
     * @return ProcedureFeaturesResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ProcedureFeaturesResponse procedureFeatures(String apiKey, String fileHash, String procRva) throws ApiException {
        ApiResponse<ProcedureFeaturesResponse> resp = procedureFeaturesWithHttpInfo(apiKey, fileHash, procRva);
        return resp.getData();
    }

    /**
     * Show the genomic features for a given procedure.
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param procRva The RVA of the procedure to find similarities with. Formatted 0xnnnn (required)
     * @return ApiResponse&lt;ProcedureFeaturesResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ProcedureFeaturesResponse> procedureFeaturesWithHttpInfo(String apiKey, String fileHash, String procRva) throws ApiException {
        com.squareup.okhttp.Call call = procedureFeaturesCall(apiKey, fileHash, procRva, null, null);
        Type localVarReturnType = new TypeToken<ProcedureFeaturesResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Show the genomic features for a given procedure. (asynchronously)
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param procRva The RVA of the procedure to find similarities with. Formatted 0xnnnn (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call procedureFeaturesAsync(String apiKey, String fileHash, String procRva, final ApiCallback<ProcedureFeaturesResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = procedureFeaturesCall(apiKey, fileHash, procRva, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ProcedureFeaturesResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
