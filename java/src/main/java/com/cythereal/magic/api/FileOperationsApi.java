

package com.cythereal.magic.api;

import io.swagger.client.ApiCallback;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.ApiResponse;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;
import io.swagger.client.ProgressRequestBody;
import io.swagger.client.ProgressResponseBody;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;

import java.io.File;
import com.cythereal.magic.model.QueryResponse;
import com.cythereal.magic.model.ResponseObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileOperationsApi {
    private ApiClient apiClient;

    public FileOperationsApi() {
        this(Configuration.getDefaultApiClient());
    }

    public FileOperationsApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /* Build call for download */
    private com.squareup.okhttp.Call downloadCall(String apiKey, String fileHash, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // verify the required parameter 'apiKey' is set
        if (apiKey == null) {
            throw new ApiException("Missing the required parameter 'apiKey' when calling download(Async)");
        }
        
        // verify the required parameter 'fileHash' is set
        if (fileHash == null) {
            throw new ApiException("Missing the required parameter 'fileHash' when calling download(Async)");
        }
        

        // create path and map variables
        String localVarPath = "/download/{api_key}/{file_hash}".replaceAll("\\{format\\}","json")
        .replaceAll("\\{" + "api_key" + "\\}", apiClient.escapeString(apiKey.toString()))
        .replaceAll("\\{" + "file_hash" + "\\}", apiClient.escapeString(fileHash.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Download a file
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @return File
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public File download(String apiKey, String fileHash) throws ApiException {
        ApiResponse<File> resp = downloadWithHttpInfo(apiKey, fileHash);
        return resp.getData();
    }

    /**
     * Download a file
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @return ApiResponse&lt;File&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<File> downloadWithHttpInfo(String apiKey, String fileHash) throws ApiException {
        com.squareup.okhttp.Call call = downloadCall(apiKey, fileHash, null, null);
        Type localVarReturnType = new TypeToken<File>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Download a file (asynchronously)
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call downloadAsync(String apiKey, String fileHash, final ApiCallback<File> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = downloadCall(apiKey, fileHash, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<File>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /* Build call for query */
    private com.squareup.okhttp.Call queryCall(String apiKey, String fileHash, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // verify the required parameter 'apiKey' is set
        if (apiKey == null) {
            throw new ApiException("Missing the required parameter 'apiKey' when calling query(Async)");
        }
        
        // verify the required parameter 'fileHash' is set
        if (fileHash == null) {
            throw new ApiException("Missing the required parameter 'fileHash' when calling query(Async)");
        }
        

        // create path and map variables
        String localVarPath = "/query/{api_key}/{file_hash}".replaceAll("\\{format\\}","json")
        .replaceAll("\\{" + "api_key" + "\\}", apiClient.escapeString(apiKey.toString()))
        .replaceAll("\\{" + "file_hash" + "\\}", apiClient.escapeString(fileHash.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Query file info and analysis status
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @return QueryResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public QueryResponse query(String apiKey, String fileHash) throws ApiException {
        ApiResponse<QueryResponse> resp = queryWithHttpInfo(apiKey, fileHash);
        return resp.getData();
    }

    /**
     * Query file info and analysis status
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @return ApiResponse&lt;QueryResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<QueryResponse> queryWithHttpInfo(String apiKey, String fileHash) throws ApiException {
        com.squareup.okhttp.Call call = queryCall(apiKey, fileHash, null, null);
        Type localVarReturnType = new TypeToken<QueryResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Query file info and analysis status (asynchronously)
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call queryAsync(String apiKey, String fileHash, final ApiCallback<QueryResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = queryCall(apiKey, fileHash, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<QueryResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /* Build call for reprocess */
    private com.squareup.okhttp.Call reprocessCall(String apiKey, String fileHash, Integer priority, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // verify the required parameter 'apiKey' is set
        if (apiKey == null) {
            throw new ApiException("Missing the required parameter 'apiKey' when calling reprocess(Async)");
        }
        
        // verify the required parameter 'fileHash' is set
        if (fileHash == null) {
            throw new ApiException("Missing the required parameter 'fileHash' when calling reprocess(Async)");
        }
        

        // create path and map variables
        String localVarPath = "/reprocess/{api_key}/{file_hash}".replaceAll("\\{format\\}","json")
        .replaceAll("\\{" + "api_key" + "\\}", apiClient.escapeString(apiKey.toString()))
        .replaceAll("\\{" + "file_hash" + "\\}", apiClient.escapeString(fileHash.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        if (priority != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "priority", priority));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Reprocess a previously uploaded file
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param priority Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. (optional, default to 2)
     * @return ResponseObject
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ResponseObject reprocess(String apiKey, String fileHash, Integer priority) throws ApiException {
        ApiResponse<ResponseObject> resp = reprocessWithHttpInfo(apiKey, fileHash, priority);
        return resp.getData();
    }

    /**
     * Reprocess a previously uploaded file
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param priority Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. (optional, default to 2)
     * @return ApiResponse&lt;ResponseObject&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ResponseObject> reprocessWithHttpInfo(String apiKey, String fileHash, Integer priority) throws ApiException {
        com.squareup.okhttp.Call call = reprocessCall(apiKey, fileHash, priority, null, null);
        Type localVarReturnType = new TypeToken<ResponseObject>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Reprocess a previously uploaded file (asynchronously)
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param priority Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. (optional, default to 2)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call reprocessAsync(String apiKey, String fileHash, Integer priority, final ApiCallback<ResponseObject> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = reprocessCall(apiKey, fileHash, priority, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ResponseObject>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /* Build call for upload */
    private com.squareup.okhttp.Call uploadCall(String apiKey, File filedata, String origFilePath, String password, Integer priority, Boolean force, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // verify the required parameter 'apiKey' is set
        if (apiKey == null) {
            throw new ApiException("Missing the required parameter 'apiKey' when calling upload(Async)");
        }
        
        // verify the required parameter 'filedata' is set
        if (filedata == null) {
            throw new ApiException("Missing the required parameter 'filedata' when calling upload(Async)");
        }
        

        // create path and map variables
        String localVarPath = "/upload/{api_key}".replaceAll("\\{format\\}","json")
        .replaceAll("\\{" + "api_key" + "\\}", apiClient.escapeString(apiKey.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        if (origFilePath != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "origFilePath", origFilePath));
        if (password != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "password", password));
        if (priority != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "priority", priority));
        if (force != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "force", force));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();
        if (filedata != null)
        localVarFormParams.put("filedata", filedata);

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "multipart/form-data"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Upload file for processing
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param filedata Binary contents of the file (required)
     * @param origFilePath Name of the file being uploaded (optional)
     * @param password If uploading a password protected zip, this field MUST contain the password. (optional)
     * @param priority Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. (optional, default to 2)
     * @param force If true, will force existing file to be reanalyzed. (optional, default to false)
     * @return ResponseObject
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ResponseObject upload(String apiKey, File filedata, String origFilePath, String password, Integer priority, Boolean force) throws ApiException {
        ApiResponse<ResponseObject> resp = uploadWithHttpInfo(apiKey, filedata, origFilePath, password, priority, force);
        return resp.getData();
    }

    /**
     * Upload file for processing
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param filedata Binary contents of the file (required)
     * @param origFilePath Name of the file being uploaded (optional)
     * @param password If uploading a password protected zip, this field MUST contain the password. (optional)
     * @param priority Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. (optional, default to 2)
     * @param force If true, will force existing file to be reanalyzed. (optional, default to false)
     * @return ApiResponse&lt;ResponseObject&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ResponseObject> uploadWithHttpInfo(String apiKey, File filedata, String origFilePath, String password, Integer priority, Boolean force) throws ApiException {
        com.squareup.okhttp.Call call = uploadCall(apiKey, filedata, origFilePath, password, priority, force, null, null);
        Type localVarReturnType = new TypeToken<ResponseObject>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Upload file for processing (asynchronously)
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param filedata Binary contents of the file (required)
     * @param origFilePath Name of the file being uploaded (optional)
     * @param password If uploading a password protected zip, this field MUST contain the password. (optional)
     * @param priority Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. (optional, default to 2)
     * @param force If true, will force existing file to be reanalyzed. (optional, default to false)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call uploadAsync(String apiKey, File filedata, String origFilePath, String password, Integer priority, Boolean force, final ApiCallback<ResponseObject> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = uploadCall(apiKey, filedata, origFilePath, password, priority, force, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ResponseObject>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
