

package com.cythereal.magic.api;

import io.swagger.client.ApiCallback;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.ApiResponse;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;
import io.swagger.client.ProgressRequestBody;
import io.swagger.client.ProgressResponseBody;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;

import com.cythereal.magic.model.BinarySimilarityResponse;
import com.cythereal.magic.model.ProcedureSimilarityResponse;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimilarityApi {
    private ApiClient apiClient;

    public SimilarityApi() {
        this(Configuration.getDefaultApiClient());
    }

    public SimilarityApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /* Build call for similarBinaries */
    private com.squareup.okhttp.Call similarBinariesCall(String apiKey, String fileHash, Float threshold, Integer level, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // verify the required parameter 'apiKey' is set
        if (apiKey == null) {
            throw new ApiException("Missing the required parameter 'apiKey' when calling similarBinaries(Async)");
        }
        
        // verify the required parameter 'fileHash' is set
        if (fileHash == null) {
            throw new ApiException("Missing the required parameter 'fileHash' when calling similarBinaries(Async)");
        }
        

        // create path and map variables
        String localVarPath = "/search/binary/{api_key}/{file_hash}".replaceAll("\\{format\\}","json")
        .replaceAll("\\{" + "api_key" + "\\}", apiClient.escapeString(apiKey.toString()))
        .replaceAll("\\{" + "file_hash" + "\\}", apiClient.escapeString(fileHash.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        if (threshold != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "threshold", threshold));
        if (level != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "level", level));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Search for similar binaries.
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param threshold Similarity threshold, default 0.7. (optional, default to 0.7)
     * @param level Similarity Search Level from 1 through 5, default is 3. (optional, default to 3)
     * @return BinarySimilarityResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public BinarySimilarityResponse similarBinaries(String apiKey, String fileHash, Float threshold, Integer level) throws ApiException {
        ApiResponse<BinarySimilarityResponse> resp = similarBinariesWithHttpInfo(apiKey, fileHash, threshold, level);
        return resp.getData();
    }

    /**
     * Search for similar binaries.
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param threshold Similarity threshold, default 0.7. (optional, default to 0.7)
     * @param level Similarity Search Level from 1 through 5, default is 3. (optional, default to 3)
     * @return ApiResponse&lt;BinarySimilarityResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<BinarySimilarityResponse> similarBinariesWithHttpInfo(String apiKey, String fileHash, Float threshold, Integer level) throws ApiException {
        com.squareup.okhttp.Call call = similarBinariesCall(apiKey, fileHash, threshold, level, null, null);
        Type localVarReturnType = new TypeToken<BinarySimilarityResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Search for similar binaries. (asynchronously)
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param threshold Similarity threshold, default 0.7. (optional, default to 0.7)
     * @param level Similarity Search Level from 1 through 5, default is 3. (optional, default to 3)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call similarBinariesAsync(String apiKey, String fileHash, Float threshold, Integer level, final ApiCallback<BinarySimilarityResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = similarBinariesCall(apiKey, fileHash, threshold, level, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<BinarySimilarityResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /* Build call for similarProcedures */
    private com.squareup.okhttp.Call similarProceduresCall(String apiKey, String fileHash, String procRva, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // verify the required parameter 'apiKey' is set
        if (apiKey == null) {
            throw new ApiException("Missing the required parameter 'apiKey' when calling similarProcedures(Async)");
        }
        
        // verify the required parameter 'fileHash' is set
        if (fileHash == null) {
            throw new ApiException("Missing the required parameter 'fileHash' when calling similarProcedures(Async)");
        }
        
        // verify the required parameter 'procRva' is set
        if (procRva == null) {
            throw new ApiException("Missing the required parameter 'procRva' when calling similarProcedures(Async)");
        }
        

        // create path and map variables
        String localVarPath = "/search/procs/{api_key}/{file_hash}/{proc_rva}".replaceAll("\\{format\\}","json")
        .replaceAll("\\{" + "api_key" + "\\}", apiClient.escapeString(apiKey.toString()))
        .replaceAll("\\{" + "file_hash" + "\\}", apiClient.escapeString(fileHash.toString()))
        .replaceAll("\\{" + "proc_rva" + "\\}", apiClient.escapeString(procRva.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Search for procedures similar to a given procedure.
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param procRva The RVA of the procedure to find similarities with. Formatted 0xnnnn (required)
     * @return ProcedureSimilarityResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ProcedureSimilarityResponse similarProcedures(String apiKey, String fileHash, String procRva) throws ApiException {
        ApiResponse<ProcedureSimilarityResponse> resp = similarProceduresWithHttpInfo(apiKey, fileHash, procRva);
        return resp.getData();
    }

    /**
     * Search for procedures similar to a given procedure.
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param procRva The RVA of the procedure to find similarities with. Formatted 0xnnnn (required)
     * @return ApiResponse&lt;ProcedureSimilarityResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ProcedureSimilarityResponse> similarProceduresWithHttpInfo(String apiKey, String fileHash, String procRva) throws ApiException {
        com.squareup.okhttp.Call call = similarProceduresCall(apiKey, fileHash, procRva, null, null);
        Type localVarReturnType = new TypeToken<ProcedureSimilarityResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Search for procedures similar to a given procedure. (asynchronously)
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param procRva The RVA of the procedure to find similarities with. Formatted 0xnnnn (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call similarProceduresAsync(String apiKey, String fileHash, String procRva, final ApiCallback<ProcedureSimilarityResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = similarProceduresCall(apiKey, fileHash, procRva, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ProcedureSimilarityResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
