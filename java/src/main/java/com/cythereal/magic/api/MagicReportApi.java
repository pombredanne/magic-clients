

package com.cythereal.magic.api;

import io.swagger.client.ApiCallback;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.ApiResponse;
import io.swagger.client.Configuration;
import io.swagger.client.Pair;
import io.swagger.client.ProgressRequestBody;
import io.swagger.client.ProgressResponseBody;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;

import com.cythereal.magic.model.MagicCategoriesResponse;
import com.cythereal.magic.model.MagicMatchesResponse;
import com.cythereal.magic.model.ResponseObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MagicReportApi {
    private ApiClient apiClient;

    public MagicReportApi() {
        this(Configuration.getDefaultApiClient());
    }

    public MagicReportApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /* Build call for magicCategories */
    private com.squareup.okhttp.Call magicCategoriesCall(String apiKey, String fileHash, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // verify the required parameter 'apiKey' is set
        if (apiKey == null) {
            throw new ApiException("Missing the required parameter 'apiKey' when calling magicCategories(Async)");
        }
        
        // verify the required parameter 'fileHash' is set
        if (fileHash == null) {
            throw new ApiException("Missing the required parameter 'fileHash' when calling magicCategories(Async)");
        }
        

        // create path and map variables
        String localVarPath = "/categories/{api_key}/{file_hash}".replaceAll("\\{format\\}","json")
        .replaceAll("\\{" + "api_key" + "\\}", apiClient.escapeString(apiKey.toString()))
        .replaceAll("\\{" + "file_hash" + "\\}", apiClient.escapeString(fileHash.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Retrieve the MAGIC categories for a binary. **Alpha Level Feature**
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @return MagicCategoriesResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public MagicCategoriesResponse magicCategories(String apiKey, String fileHash) throws ApiException {
        ApiResponse<MagicCategoriesResponse> resp = magicCategoriesWithHttpInfo(apiKey, fileHash);
        return resp.getData();
    }

    /**
     * Retrieve the MAGIC categories for a binary. **Alpha Level Feature**
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @return ApiResponse&lt;MagicCategoriesResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<MagicCategoriesResponse> magicCategoriesWithHttpInfo(String apiKey, String fileHash) throws ApiException {
        com.squareup.okhttp.Call call = magicCategoriesCall(apiKey, fileHash, null, null);
        Type localVarReturnType = new TypeToken<MagicCategoriesResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve the MAGIC categories for a binary. **Alpha Level Feature** (asynchronously)
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call magicCategoriesAsync(String apiKey, String fileHash, final ApiCallback<MagicCategoriesResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = magicCategoriesCall(apiKey, fileHash, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<MagicCategoriesResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /* Build call for magicCorrelations */
    private com.squareup.okhttp.Call magicCorrelationsCall(String apiKey, String fileHash, Boolean details, Float threshold, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // verify the required parameter 'apiKey' is set
        if (apiKey == null) {
            throw new ApiException("Missing the required parameter 'apiKey' when calling magicCorrelations(Async)");
        }
        
        // verify the required parameter 'fileHash' is set
        if (fileHash == null) {
            throw new ApiException("Missing the required parameter 'fileHash' when calling magicCorrelations(Async)");
        }
        

        // create path and map variables
        String localVarPath = "/magic/{api_key}/{file_hash}".replaceAll("\\{format\\}","json")
        .replaceAll("\\{" + "api_key" + "\\}", apiClient.escapeString(apiKey.toString()))
        .replaceAll("\\{" + "file_hash" + "\\}", apiClient.escapeString(fileHash.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        if (details != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "details", details));
        if (threshold != null)
        localVarQueryParams.addAll(apiClient.parameterToPairs("", "threshold", threshold));

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Retrieve the MAGIC correlations for a binary.
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param details If false, omits the match details. Defaults to true. (optional, default to true)
     * @param threshold Only similarity matches at value equal or above threshold will be considered. MUST be in range [0, 1]. Defaults to 0.7. (optional, default to 0.7)
     * @return MagicMatchesResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public MagicMatchesResponse magicCorrelations(String apiKey, String fileHash, Boolean details, Float threshold) throws ApiException {
        ApiResponse<MagicMatchesResponse> resp = magicCorrelationsWithHttpInfo(apiKey, fileHash, details, threshold);
        return resp.getData();
    }

    /**
     * Retrieve the MAGIC correlations for a binary.
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param details If false, omits the match details. Defaults to true. (optional, default to true)
     * @param threshold Only similarity matches at value equal or above threshold will be considered. MUST be in range [0, 1]. Defaults to 0.7. (optional, default to 0.7)
     * @return ApiResponse&lt;MagicMatchesResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<MagicMatchesResponse> magicCorrelationsWithHttpInfo(String apiKey, String fileHash, Boolean details, Float threshold) throws ApiException {
        com.squareup.okhttp.Call call = magicCorrelationsCall(apiKey, fileHash, details, threshold, null, null);
        Type localVarReturnType = new TypeToken<MagicMatchesResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve the MAGIC correlations for a binary. (asynchronously)
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param details If false, omits the match details. Defaults to true. (optional, default to true)
     * @param threshold Only similarity matches at value equal or above threshold will be considered. MUST be in range [0, 1]. Defaults to 0.7. (optional, default to 0.7)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call magicCorrelationsAsync(String apiKey, String fileHash, Boolean details, Float threshold, final ApiCallback<MagicMatchesResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = magicCorrelationsCall(apiKey, fileHash, details, threshold, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<MagicMatchesResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /* Build call for putMagicCategories */
    private com.squareup.okhttp.Call putMagicCategoriesCall(String apiKey, String fileHash, String category, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // verify the required parameter 'apiKey' is set
        if (apiKey == null) {
            throw new ApiException("Missing the required parameter 'apiKey' when calling putMagicCategories(Async)");
        }
        
        // verify the required parameter 'fileHash' is set
        if (fileHash == null) {
            throw new ApiException("Missing the required parameter 'fileHash' when calling putMagicCategories(Async)");
        }
        
        // verify the required parameter 'category' is set
        if (category == null) {
            throw new ApiException("Missing the required parameter 'category' when calling putMagicCategories(Async)");
        }
        

        // create path and map variables
        String localVarPath = "/categories/{api_key}/{file_hash}".replaceAll("\\{format\\}","json")
        .replaceAll("\\{" + "api_key" + "\\}", apiClient.escapeString(apiKey.toString()))
        .replaceAll("\\{" + "file_hash" + "\\}", apiClient.escapeString(fileHash.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();
        if (category != null)
        localVarFormParams.put("category", category);

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/x-www-form-urlencoded"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "PUT", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    /**
     * Save category for a binary. **Alpha Level Feature**
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param category Category assigned to the file. (required)
     * @return ResponseObject
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ResponseObject putMagicCategories(String apiKey, String fileHash, String category) throws ApiException {
        ApiResponse<ResponseObject> resp = putMagicCategoriesWithHttpInfo(apiKey, fileHash, category);
        return resp.getData();
    }

    /**
     * Save category for a binary. **Alpha Level Feature**
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param category Category assigned to the file. (required)
     * @return ApiResponse&lt;ResponseObject&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ResponseObject> putMagicCategoriesWithHttpInfo(String apiKey, String fileHash, String category) throws ApiException {
        com.squareup.okhttp.Call call = putMagicCategoriesCall(apiKey, fileHash, category, null, null);
        Type localVarReturnType = new TypeToken<ResponseObject>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Save category for a binary. **Alpha Level Feature** (asynchronously)
     * 
     * @param apiKey API Key for accessing the service (required)
     * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1. (required)
     * @param category Category assigned to the file. (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call putMagicCategoriesAsync(String apiKey, String fileHash, String category, final ApiCallback<ResponseObject> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = putMagicCategoriesCall(apiKey, fileHash, category, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ResponseObject>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
