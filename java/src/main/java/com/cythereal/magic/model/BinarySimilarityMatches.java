package com.cythereal.magic.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * BinarySimilarityMatches
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class BinarySimilarityMatches {
  @SerializedName("fileHash")
  private String fileHash = null;

  @SerializedName("similarity")
  private Float similarity = null;

  public BinarySimilarityMatches fileHash(String fileHash) {
    this.fileHash = fileHash;
    return this;
  }

   /**
   * SHA1 of a similar binary
   * @return fileHash
  **/
  @ApiModelProperty(value = "SHA1 of a similar binary")
  public String getFileHash() {
    return fileHash;
  }

  public void setFileHash(String fileHash) {
    this.fileHash = fileHash;
  }

  public BinarySimilarityMatches similarity(Float similarity) {
    this.similarity = similarity;
    return this;
  }

   /**
   * Similarity score of the matched binary
   * @return similarity
  **/
  @ApiModelProperty(value = "Similarity score of the matched binary")
  public Float getSimilarity() {
    return similarity;
  }

  public void setSimilarity(Float similarity) {
    this.similarity = similarity;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BinarySimilarityMatches binarySimilarityMatches = (BinarySimilarityMatches) o;
    return Objects.equals(this.fileHash, binarySimilarityMatches.fileHash) &&
        Objects.equals(this.similarity, binarySimilarityMatches.similarity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fileHash, similarity);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BinarySimilarityMatches {\n");
    
    sb.append("    fileHash: ").append(toIndentedString(fileHash)).append("\n");
    sb.append("    similarity: ").append(toIndentedString(similarity)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

