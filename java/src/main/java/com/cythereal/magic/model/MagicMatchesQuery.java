package com.cythereal.magic.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Information about the binary used to make this query.
 */
@ApiModel(description = "Information about the binary used to make this query.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class MagicMatchesQuery {
  @SerializedName("sha1")
  private String sha1 = null;

  public MagicMatchesQuery sha1(String sha1) {
    this.sha1 = sha1;
    return this;
  }

   /**
   * Get sha1
   * @return sha1
  **/
  @ApiModelProperty(example = "2d9035b27ce5c1cce2fb8432c76e315a1a2a8de0", required = true, value = "")
  public String getSha1() {
    return sha1;
  }

  public void setSha1(String sha1) {
    this.sha1 = sha1;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MagicMatchesQuery magicMatchesQuery = (MagicMatchesQuery) o;
    return Objects.equals(this.sha1, magicMatchesQuery.sha1);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sha1);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MagicMatchesQuery {\n");
    
    sb.append("    sha1: ").append(toIndentedString(sha1)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

