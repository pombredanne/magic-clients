package com.cythereal.magic.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * The SHA1 of all MAGIC matches, separated by type of MAGIC match.
 */
@ApiModel(description = "The SHA1 of all MAGIC matches, separated by type of MAGIC match.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class MagicMatchesSummary {
  @SerializedName("similar_packer_similar_payload")
  private List<String> similarPackerSimilarPayload = new ArrayList<String>();

  @SerializedName("similar_packer_different_payload")
  private List<String> similarPackerDifferentPayload = new ArrayList<String>();

  @SerializedName("different_packer_similar_payload")
  private List<String> differentPackerSimilarPayload = new ArrayList<String>();

  @SerializedName("weak_similar")
  private List<String> weakSimilar = new ArrayList<String>();

  public MagicMatchesSummary similarPackerSimilarPayload(List<String> similarPackerSimilarPayload) {
    this.similarPackerSimilarPayload = similarPackerSimilarPayload;
    return this;
  }

  public MagicMatchesSummary addSimilarPackerSimilarPayloadItem(String similarPackerSimilarPayloadItem) {
    this.similarPackerSimilarPayload.add(similarPackerSimilarPayloadItem);
    return this;
  }

   /**
   * Get similarPackerSimilarPayload
   * @return similarPackerSimilarPayload
  **/
  @ApiModelProperty(example = "[&quot;01ab20c6bb66f5e393f53dd7f54bd2d829745a6e&quot;]", value = "")
  public List<String> getSimilarPackerSimilarPayload() {
    return similarPackerSimilarPayload;
  }

  public void setSimilarPackerSimilarPayload(List<String> similarPackerSimilarPayload) {
    this.similarPackerSimilarPayload = similarPackerSimilarPayload;
  }

  public MagicMatchesSummary similarPackerDifferentPayload(List<String> similarPackerDifferentPayload) {
    this.similarPackerDifferentPayload = similarPackerDifferentPayload;
    return this;
  }

  public MagicMatchesSummary addSimilarPackerDifferentPayloadItem(String similarPackerDifferentPayloadItem) {
    this.similarPackerDifferentPayload.add(similarPackerDifferentPayloadItem);
    return this;
  }

   /**
   * Get similarPackerDifferentPayload
   * @return similarPackerDifferentPayload
  **/
  @ApiModelProperty(example = "[&quot;008019b0ebf88cbcc4feab0347e3e823fdb1a372&quot;,&quot;fcf5100bc222fc21b7326bb9a99d1aeac83a0f3e&quot;]", value = "")
  public List<String> getSimilarPackerDifferentPayload() {
    return similarPackerDifferentPayload;
  }

  public void setSimilarPackerDifferentPayload(List<String> similarPackerDifferentPayload) {
    this.similarPackerDifferentPayload = similarPackerDifferentPayload;
  }

  public MagicMatchesSummary differentPackerSimilarPayload(List<String> differentPackerSimilarPayload) {
    this.differentPackerSimilarPayload = differentPackerSimilarPayload;
    return this;
  }

  public MagicMatchesSummary addDifferentPackerSimilarPayloadItem(String differentPackerSimilarPayloadItem) {
    this.differentPackerSimilarPayload.add(differentPackerSimilarPayloadItem);
    return this;
  }

   /**
   * Get differentPackerSimilarPayload
   * @return differentPackerSimilarPayload
  **/
  @ApiModelProperty(example = "[&quot;67120136e48d1307470e22c3a26d3a94bb843d36&quot;]", value = "")
  public List<String> getDifferentPackerSimilarPayload() {
    return differentPackerSimilarPayload;
  }

  public void setDifferentPackerSimilarPayload(List<String> differentPackerSimilarPayload) {
    this.differentPackerSimilarPayload = differentPackerSimilarPayload;
  }

  public MagicMatchesSummary weakSimilar(List<String> weakSimilar) {
    this.weakSimilar = weakSimilar;
    return this;
  }

  public MagicMatchesSummary addWeakSimilarItem(String weakSimilarItem) {
    this.weakSimilar.add(weakSimilarItem);
    return this;
  }

   /**
   * Get weakSimilar
   * @return weakSimilar
  **/
  @ApiModelProperty(example = "[&quot;0f5c62c1032beb536aa549930e37a83ade94a867&quot;]", value = "")
  public List<String> getWeakSimilar() {
    return weakSimilar;
  }

  public void setWeakSimilar(List<String> weakSimilar) {
    this.weakSimilar = weakSimilar;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MagicMatchesSummary magicMatchesSummary = (MagicMatchesSummary) o;
    return Objects.equals(this.similarPackerSimilarPayload, magicMatchesSummary.similarPackerSimilarPayload) &&
        Objects.equals(this.similarPackerDifferentPayload, magicMatchesSummary.similarPackerDifferentPayload) &&
        Objects.equals(this.differentPackerSimilarPayload, magicMatchesSummary.differentPackerSimilarPayload) &&
        Objects.equals(this.weakSimilar, magicMatchesSummary.weakSimilar);
  }

  @Override
  public int hashCode() {
    return Objects.hash(similarPackerSimilarPayload, similarPackerDifferentPayload, differentPackerSimilarPayload, weakSimilar);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MagicMatchesSummary {\n");
    
    sb.append("    similarPackerSimilarPayload: ").append(toIndentedString(similarPackerSimilarPayload)).append("\n");
    sb.append("    similarPackerDifferentPayload: ").append(toIndentedString(similarPackerDifferentPayload)).append("\n");
    sb.append("    differentPackerSimilarPayload: ").append(toIndentedString(differentPackerSimilarPayload)).append("\n");
    sb.append("    weakSimilar: ").append(toIndentedString(weakSimilar)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

