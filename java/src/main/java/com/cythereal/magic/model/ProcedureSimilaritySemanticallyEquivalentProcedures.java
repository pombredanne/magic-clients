package com.cythereal.magic.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ProcedureSimilaritySemanticallyEquivalentProcedures
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class ProcedureSimilaritySemanticallyEquivalentProcedures {
  @SerializedName("procName")
  private String procName = null;

  @SerializedName("proc_id")
  private String procId = null;

  public ProcedureSimilaritySemanticallyEquivalentProcedures procName(String procName) {
    this.procName = procName;
    return this;
  }

   /**
   * Name of the matched procedure.
   * @return procName
  **/
  @ApiModelProperty(value = "Name of the matched procedure.")
  public String getProcName() {
    return procName;
  }

  public void setProcName(String procName) {
    this.procName = procName;
  }

  public ProcedureSimilaritySemanticallyEquivalentProcedures procId(String procId) {
    this.procId = procId;
    return this;
  }

   /**
   * ID of the matched procedure.
   * @return procId
  **/
  @ApiModelProperty(value = "ID of the matched procedure.")
  public String getProcId() {
    return procId;
  }

  public void setProcId(String procId) {
    this.procId = procId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProcedureSimilaritySemanticallyEquivalentProcedures procedureSimilaritySemanticallyEquivalentProcedures = (ProcedureSimilaritySemanticallyEquivalentProcedures) o;
    return Objects.equals(this.procName, procedureSimilaritySemanticallyEquivalentProcedures.procName) &&
        Objects.equals(this.procId, procedureSimilaritySemanticallyEquivalentProcedures.procId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(procName, procId);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProcedureSimilaritySemanticallyEquivalentProcedures {\n");
    
    sb.append("    procName: ").append(toIndentedString(procName)).append("\n");
    sb.append("    procId: ").append(toIndentedString(procId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

