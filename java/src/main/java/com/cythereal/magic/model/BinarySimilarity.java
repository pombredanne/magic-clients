package com.cythereal.magic.model;

import java.util.Objects;
import com.cythereal.magic.model.BinarySimilarityMatches;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * BinarySimilarity
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class BinarySimilarity {
  @SerializedName("matches")
  private List<BinarySimilarityMatches> matches = new ArrayList<BinarySimilarityMatches>();

  public BinarySimilarity matches(List<BinarySimilarityMatches> matches) {
    this.matches = matches;
    return this;
  }

  public BinarySimilarity addMatchesItem(BinarySimilarityMatches matchesItem) {
    this.matches.add(matchesItem);
    return this;
  }

   /**
   * List of binaries similar to the query binary. Sorted by decreasing similarity.
   * @return matches
  **/
  @ApiModelProperty(value = "List of binaries similar to the query binary. Sorted by decreasing similarity.")
  public List<BinarySimilarityMatches> getMatches() {
    return matches;
  }

  public void setMatches(List<BinarySimilarityMatches> matches) {
    this.matches = matches;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BinarySimilarity binarySimilarity = (BinarySimilarity) o;
    return Objects.equals(this.matches, binarySimilarity.matches);
  }

  @Override
  public int hashCode() {
    return Objects.hash(matches);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BinarySimilarity {\n");
    
    sb.append("    matches: ").append(toIndentedString(matches)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

