package com.cythereal.magic.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ResponseObject
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class ResponseObject {
  /**
   * Indicates whether an operation was successful or not.
   */
  public enum StatusEnum {
    @SerializedName("success")
    SUCCESS("success"),
    
    @SerializedName("failure")
    FAILURE("failure");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
  }

  @SerializedName("status")
  private StatusEnum status = null;

  @SerializedName("statuscode")
  private Integer statuscode = null;

  @SerializedName("vb_version")
  private String vbVersion = null;

  @SerializedName("message")
  private String message = null;

  @SerializedName("hash")
  private String hash = null;

  public ResponseObject status(StatusEnum status) {
    this.status = status;
    return this;
  }

   /**
   * Indicates whether an operation was successful or not.
   * @return status
  **/
  @ApiModelProperty(example = "success", required = true, value = "Indicates whether an operation was successful or not.")
  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public ResponseObject statuscode(Integer statuscode) {
    this.statuscode = statuscode;
    return this;
  }

   /**
   * Status code for the operation. This is NOT the same as an HTTP status code. It is closer to a unix exit code. Zero indicates success. Non-zero indicates failure.
   * @return statuscode
  **/
  @ApiModelProperty(example = "0", required = true, value = "Status code for the operation. This is NOT the same as an HTTP status code. It is closer to a unix exit code. Zero indicates success. Non-zero indicates failure.")
  public Integer getStatuscode() {
    return statuscode;
  }

  public void setStatuscode(Integer statuscode) {
    this.statuscode = statuscode;
  }

  public ResponseObject vbVersion(String vbVersion) {
    this.vbVersion = vbVersion;
    return this;
  }

   /**
   * 
   * @return vbVersion
  **/
  @ApiModelProperty(example = "VB-0.4", required = true, value = "")
  public String getVbVersion() {
    return vbVersion;
  }

  public void setVbVersion(String vbVersion) {
    this.vbVersion = vbVersion;
  }

  public ResponseObject message(String message) {
    this.message = message;
    return this;
  }

   /**
   * A message for the user. Current version of the API.
   * @return message
  **/
  @ApiModelProperty(value = "A message for the user. Current version of the API.")
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public ResponseObject hash(String hash) {
    this.hash = hash;
    return this;
  }

   /**
   * The hash value used in the query.
   * @return hash
  **/
  @ApiModelProperty(value = "The hash value used in the query.")
  public String getHash() {
    return hash;
  }

  public void setHash(String hash) {
    this.hash = hash;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ResponseObject responseObject = (ResponseObject) o;
    return Objects.equals(this.status, responseObject.status) &&
        Objects.equals(this.statuscode, responseObject.statuscode) &&
        Objects.equals(this.vbVersion, responseObject.vbVersion) &&
        Objects.equals(this.message, responseObject.message) &&
        Objects.equals(this.hash, responseObject.hash);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, statuscode, vbVersion, message, hash);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ResponseObject {\n");
    
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    statuscode: ").append(toIndentedString(statuscode)).append("\n");
    sb.append("    vbVersion: ").append(toIndentedString(vbVersion)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("    hash: ").append(toIndentedString(hash)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

