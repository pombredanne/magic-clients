package com.cythereal.magic.model;

import java.util.Objects;
import com.cythereal.magic.model.MagicCategoriesCategorizationResult;
import com.cythereal.magic.model.MagicCategoriesGroundTruth;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The object containing categories correlated with a query binary.
 */
@ApiModel(description = "The object containing categories correlated with a query binary.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class MagicCategories {
  @SerializedName("ground_truth")
  private MagicCategoriesGroundTruth groundTruth = null;

  @SerializedName("categorization_result")
  private MagicCategoriesCategorizationResult categorizationResult = null;

  public MagicCategories groundTruth(MagicCategoriesGroundTruth groundTruth) {
    this.groundTruth = groundTruth;
    return this;
  }

   /**
   * Get groundTruth
   * @return groundTruth
  **/
  @ApiModelProperty(value = "")
  public MagicCategoriesGroundTruth getGroundTruth() {
    return groundTruth;
  }

  public void setGroundTruth(MagicCategoriesGroundTruth groundTruth) {
    this.groundTruth = groundTruth;
  }

  public MagicCategories categorizationResult(MagicCategoriesCategorizationResult categorizationResult) {
    this.categorizationResult = categorizationResult;
    return this;
  }

   /**
   * Get categorizationResult
   * @return categorizationResult
  **/
  @ApiModelProperty(value = "")
  public MagicCategoriesCategorizationResult getCategorizationResult() {
    return categorizationResult;
  }

  public void setCategorizationResult(MagicCategoriesCategorizationResult categorizationResult) {
    this.categorizationResult = categorizationResult;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MagicCategories magicCategories = (MagicCategories) o;
    return Objects.equals(this.groundTruth, magicCategories.groundTruth) &&
        Objects.equals(this.categorizationResult, magicCategories.categorizationResult);
  }

  @Override
  public int hashCode() {
    return Objects.hash(groundTruth, categorizationResult);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MagicCategories {\n");
    
    sb.append("    groundTruth: ").append(toIndentedString(groundTruth)).append("\n");
    sb.append("    categorizationResult: ").append(toIndentedString(categorizationResult)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

