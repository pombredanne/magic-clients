package com.cythereal.magic.model;

import java.util.Objects;
import com.cythereal.magic.model.ProcedureSimilaritySemanticallyEquivalentProcedures;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * List of procedures found to be similar to a given procedure.
 */
@ApiModel(description = "List of procedures found to be similar to a given procedure.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class ProcedureSimilarity {
  @SerializedName("semantically_equivalent_procedures")
  private List<ProcedureSimilaritySemanticallyEquivalentProcedures> semanticallyEquivalentProcedures = new ArrayList<ProcedureSimilaritySemanticallyEquivalentProcedures>();

  public ProcedureSimilarity semanticallyEquivalentProcedures(List<ProcedureSimilaritySemanticallyEquivalentProcedures> semanticallyEquivalentProcedures) {
    this.semanticallyEquivalentProcedures = semanticallyEquivalentProcedures;
    return this;
  }

  public ProcedureSimilarity addSemanticallyEquivalentProceduresItem(ProcedureSimilaritySemanticallyEquivalentProcedures semanticallyEquivalentProceduresItem) {
    this.semanticallyEquivalentProcedures.add(semanticallyEquivalentProceduresItem);
    return this;
  }

   /**
   * List of procedures with the exact semantics as the given procedure.
   * @return semanticallyEquivalentProcedures
  **/
  @ApiModelProperty(value = "List of procedures with the exact semantics as the given procedure.")
  public List<ProcedureSimilaritySemanticallyEquivalentProcedures> getSemanticallyEquivalentProcedures() {
    return semanticallyEquivalentProcedures;
  }

  public void setSemanticallyEquivalentProcedures(List<ProcedureSimilaritySemanticallyEquivalentProcedures> semanticallyEquivalentProcedures) {
    this.semanticallyEquivalentProcedures = semanticallyEquivalentProcedures;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProcedureSimilarity procedureSimilarity = (ProcedureSimilarity) o;
    return Objects.equals(this.semanticallyEquivalentProcedures, procedureSimilarity.semanticallyEquivalentProcedures);
  }

  @Override
  public int hashCode() {
    return Objects.hash(semanticallyEquivalentProcedures);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProcedureSimilarity {\n");
    
    sb.append("    semanticallyEquivalentProcedures: ").append(toIndentedString(semanticallyEquivalentProcedures)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

