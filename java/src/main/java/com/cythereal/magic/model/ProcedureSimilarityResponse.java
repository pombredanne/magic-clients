package com.cythereal.magic.model;

import java.util.Objects;
import com.cythereal.magic.model.ProcedureSimilarity;
import com.cythereal.magic.model.ResponseObject;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * ProcedureSimilarityResponse
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class ProcedureSimilarityResponse extends ResponseObject {
  @SerializedName("answer")
  private ProcedureSimilarity answer = null;

  public ProcedureSimilarityResponse answer(ProcedureSimilarity answer) {
    this.answer = answer;
    return this;
  }

   /**
   * Get answer
   * @return answer
  **/
  @ApiModelProperty(value = "")
  public ProcedureSimilarity getAnswer() {
    return answer;
  }

  public void setAnswer(ProcedureSimilarity answer) {
    this.answer = answer;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProcedureSimilarityResponse procedureSimilarityResponse = (ProcedureSimilarityResponse) o;
    return Objects.equals(this.answer, procedureSimilarityResponse.answer) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(answer, super.hashCode());
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProcedureSimilarityResponse {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    answer: ").append(toIndentedString(answer)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

