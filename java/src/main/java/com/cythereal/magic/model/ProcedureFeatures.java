package com.cythereal.magic.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * The genomic features for a single procedure.
 */
@ApiModel(description = "The genomic features for a single procedure.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class ProcedureFeatures {
  @SerializedName("_id")
  private String id = null;

  @SerializedName("binary_id")
  private String binaryId = null;

  @SerializedName("hardHash")
  private String hardHash = null;

  @SerializedName("startRVA")
  private String startRVA = null;

  @SerializedName("isLibrary")
  private Boolean isLibrary = null;

  @SerializedName("isThunk")
  private Boolean isThunk = null;

  @SerializedName("peSegment")
  private String peSegment = null;

  @SerializedName("procName")
  private String procName = null;

  @SerializedName("api_calls")
  private List<String> apiCalls = new ArrayList<String>();

  @SerializedName("code")
  private List<List<String>> code = new ArrayList<List<String>>();

  @SerializedName("code_size")
  private Integer codeSize = null;

  @SerializedName("gen_code")
  private List<List<String>> genCode = new ArrayList<List<String>>();

  @SerializedName("gen_code_size")
  private Integer genCodeSize = null;

  @SerializedName("semantics")
  private List<List<String>> semantics = new ArrayList<List<String>>();

  @SerializedName("semantics_size")
  private Integer semanticsSize = null;

  @SerializedName("gen_semantics")
  private List<List<String>> genSemantics = new ArrayList<List<String>>();

  @SerializedName("gen_semantics_size")
  private Integer genSemanticsSize = null;

  public ProcedureFeatures id(String id) {
    this.id = id;
    return this;
  }

   /**
   * The ID of the function. It takes the form \"`binary_id`/`startRVA`\".
   * @return id
  **/
  @ApiModelProperty(value = "The ID of the function. It takes the form \"`binary_id`/`startRVA`\".")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public ProcedureFeatures binaryId(String binaryId) {
    this.binaryId = binaryId;
    return this;
  }

   /**
   * The SHA1 of the binary this function is in.
   * @return binaryId
  **/
  @ApiModelProperty(value = "The SHA1 of the binary this function is in.")
  public String getBinaryId() {
    return binaryId;
  }

  public void setBinaryId(String binaryId) {
    this.binaryId = binaryId;
  }

  public ProcedureFeatures hardHash(String hardHash) {
    this.hardHash = hardHash;
    return this;
  }

   /**
   * This is a special hash created by MAGIC to identify semantically equivalent functions. If two functions have an equivalent hardHash, then the semantics of both functions are the same, i.e. the functions can be considered functionally equivalent.
   * @return hardHash
  **/
  @ApiModelProperty(value = "This is a special hash created by MAGIC to identify semantically equivalent functions. If two functions have an equivalent hardHash, then the semantics of both functions are the same, i.e. the functions can be considered functionally equivalent.")
  public String getHardHash() {
    return hardHash;
  }

  public void setHardHash(String hardHash) {
    this.hardHash = hardHash;
  }

  public ProcedureFeatures startRVA(String startRVA) {
    this.startRVA = startRVA;
    return this;
  }

   /**
   * The relative virtual address of the function
   * @return startRVA
  **/
  @ApiModelProperty(value = "The relative virtual address of the function")
  public String getStartRVA() {
    return startRVA;
  }

  public void setStartRVA(String startRVA) {
    this.startRVA = startRVA;
  }

  public ProcedureFeatures isLibrary(Boolean isLibrary) {
    this.isLibrary = isLibrary;
    return this;
  }

   /**
   * Indicates  if this function was identified as a library function.
   * @return isLibrary
  **/
  @ApiModelProperty(value = "Indicates  if this function was identified as a library function.")
  public Boolean getIsLibrary() {
    return isLibrary;
  }

  public void setIsLibrary(Boolean isLibrary) {
    this.isLibrary = isLibrary;
  }

  public ProcedureFeatures isThunk(Boolean isThunk) {
    this.isThunk = isThunk;
    return this;
  }

   /**
   * Indicates if this function was identified as a thunk (jump) function.
   * @return isThunk
  **/
  @ApiModelProperty(value = "Indicates if this function was identified as a thunk (jump) function.")
  public Boolean getIsThunk() {
    return isThunk;
  }

  public void setIsThunk(Boolean isThunk) {
    this.isThunk = isThunk;
  }

  public ProcedureFeatures peSegment(String peSegment) {
    this.peSegment = peSegment;
    return this;
  }

   /**
   * Lists the [PE Segment](https://msdn.microsoft.com/en-us/library/ms809762.aspx) that the function was located in.
   * @return peSegment
  **/
  @ApiModelProperty(value = "Lists the [PE Segment](https://msdn.microsoft.com/en-us/library/ms809762.aspx) that the function was located in.")
  public String getPeSegment() {
    return peSegment;
  }

  public void setPeSegment(String peSegment) {
    this.peSegment = peSegment;
  }

  public ProcedureFeatures procName(String procName) {
    this.procName = procName;
    return this;
  }

   /**
   * The name of the procedure. If debug symbols were not present in the original file, this will be of the form `sub_xxxxxx` where `xxxxxx` is the virtual address of the function.
   * @return procName
  **/
  @ApiModelProperty(value = "The name of the procedure. If debug symbols were not present in the original file, this will be of the form `sub_xxxxxx` where `xxxxxx` is the virtual address of the function.")
  public String getProcName() {
    return procName;
  }

  public void setProcName(String procName) {
    this.procName = procName;
  }

  public ProcedureFeatures apiCalls(List<String> apiCalls) {
    this.apiCalls = apiCalls;
    return this;
  }

  public ProcedureFeatures addApiCallsItem(String apiCallsItem) {
    this.apiCalls.add(apiCallsItem);
    return this;
  }

   /**
   * The list of APIs called from this function.
   * @return apiCalls
  **/
  @ApiModelProperty(value = "The list of APIs called from this function.")
  public List<String> getApiCalls() {
    return apiCalls;
  }

  public void setApiCalls(List<String> apiCalls) {
    this.apiCalls = apiCalls;
  }

  public ProcedureFeatures code(List<List<String>> code) {
    this.code = code;
    return this;
  }

  public ProcedureFeatures addCodeItem(List<String> codeItem) {
    this.code.add(codeItem);
    return this;
  }

   /**
   * The disassembly of this function. This is a list of lists where each sublist represents a disassembly block. Each block contains a list of the instructions that compose the block.
   * @return code
  **/
  @ApiModelProperty(value = "The disassembly of this function. This is a list of lists where each sublist represents a disassembly block. Each block contains a list of the instructions that compose the block.")
  public List<List<String>> getCode() {
    return code;
  }

  public void setCode(List<List<String>> code) {
    this.code = code;
  }

  public ProcedureFeatures codeSize(Integer codeSize) {
    this.codeSize = codeSize;
    return this;
  }

   /**
   * The number of instructions in the disassembly.
   * @return codeSize
  **/
  @ApiModelProperty(value = "The number of instructions in the disassembly.")
  public Integer getCodeSize() {
    return codeSize;
  }

  public void setCodeSize(Integer codeSize) {
    this.codeSize = codeSize;
  }

  public ProcedureFeatures genCode(List<List<String>> genCode) {
    this.genCode = genCode;
    return this;
  }

  public ProcedureFeatures addGenCodeItem(List<String> genCodeItem) {
    this.genCode.add(genCodeItem);
    return this;
  }

   /**
   * Generalized disassembly. This is the same as `code`, but with the operands in code abstracted to variables instead of register names.
   * @return genCode
  **/
  @ApiModelProperty(value = "Generalized disassembly. This is the same as `code`, but with the operands in code abstracted to variables instead of register names.")
  public List<List<String>> getGenCode() {
    return genCode;
  }

  public void setGenCode(List<List<String>> genCode) {
    this.genCode = genCode;
  }

  public ProcedureFeatures genCodeSize(Integer genCodeSize) {
    this.genCodeSize = genCodeSize;
    return this;
  }

   /**
   * The number of generalized instructions in `gen_code`.
   * @return genCodeSize
  **/
  @ApiModelProperty(value = "The number of generalized instructions in `gen_code`.")
  public Integer getGenCodeSize() {
    return genCodeSize;
  }

  public void setGenCodeSize(Integer genCodeSize) {
    this.genCodeSize = genCodeSize;
  }

  public ProcedureFeatures semantics(List<List<String>> semantics) {
    this.semantics = semantics;
    return this;
  }

  public ProcedureFeatures addSemanticsItem(List<String> semanticsItem) {
    this.semantics.add(semanticsItem);
    return this;
  }

   /**
   * The semantics of the instructions in `code`. This is a list of lists where each inner list corresponds to a block from `code`. This inner list contains the effect that executing the represented block will have on registers and memory locations.
   * @return semantics
  **/
  @ApiModelProperty(value = "The semantics of the instructions in `code`. This is a list of lists where each inner list corresponds to a block from `code`. This inner list contains the effect that executing the represented block will have on registers and memory locations.")
  public List<List<String>> getSemantics() {
    return semantics;
  }

  public void setSemantics(List<List<String>> semantics) {
    this.semantics = semantics;
  }

  public ProcedureFeatures semanticsSize(Integer semanticsSize) {
    this.semanticsSize = semanticsSize;
    return this;
  }

   /**
   * The number of individual semantics statements in `semantics`
   * @return semanticsSize
  **/
  @ApiModelProperty(value = "The number of individual semantics statements in `semantics`")
  public Integer getSemanticsSize() {
    return semanticsSize;
  }

  public void setSemanticsSize(Integer semanticsSize) {
    this.semanticsSize = semanticsSize;
  }

  public ProcedureFeatures genSemantics(List<List<String>> genSemantics) {
    this.genSemantics = genSemantics;
    return this;
  }

  public ProcedureFeatures addGenSemanticsItem(List<String> genSemanticsItem) {
    this.genSemantics.add(genSemanticsItem);
    return this;
  }

   /**
   * Generalized semantics. This is the same as `semantics` but with registers and memory locations abstracted to variables.
   * @return genSemantics
  **/
  @ApiModelProperty(value = "Generalized semantics. This is the same as `semantics` but with registers and memory locations abstracted to variables.")
  public List<List<String>> getGenSemantics() {
    return genSemantics;
  }

  public void setGenSemantics(List<List<String>> genSemantics) {
    this.genSemantics = genSemantics;
  }

  public ProcedureFeatures genSemanticsSize(Integer genSemanticsSize) {
    this.genSemanticsSize = genSemanticsSize;
    return this;
  }

   /**
   * The number of individual semantics statements in `gen_semantics`
   * @return genSemanticsSize
  **/
  @ApiModelProperty(value = "The number of individual semantics statements in `gen_semantics`")
  public Integer getGenSemanticsSize() {
    return genSemanticsSize;
  }

  public void setGenSemanticsSize(Integer genSemanticsSize) {
    this.genSemanticsSize = genSemanticsSize;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProcedureFeatures procedureFeatures = (ProcedureFeatures) o;
    return Objects.equals(this.id, procedureFeatures.id) &&
        Objects.equals(this.binaryId, procedureFeatures.binaryId) &&
        Objects.equals(this.hardHash, procedureFeatures.hardHash) &&
        Objects.equals(this.startRVA, procedureFeatures.startRVA) &&
        Objects.equals(this.isLibrary, procedureFeatures.isLibrary) &&
        Objects.equals(this.isThunk, procedureFeatures.isThunk) &&
        Objects.equals(this.peSegment, procedureFeatures.peSegment) &&
        Objects.equals(this.procName, procedureFeatures.procName) &&
        Objects.equals(this.apiCalls, procedureFeatures.apiCalls) &&
        Objects.equals(this.code, procedureFeatures.code) &&
        Objects.equals(this.codeSize, procedureFeatures.codeSize) &&
        Objects.equals(this.genCode, procedureFeatures.genCode) &&
        Objects.equals(this.genCodeSize, procedureFeatures.genCodeSize) &&
        Objects.equals(this.semantics, procedureFeatures.semantics) &&
        Objects.equals(this.semanticsSize, procedureFeatures.semanticsSize) &&
        Objects.equals(this.genSemantics, procedureFeatures.genSemantics) &&
        Objects.equals(this.genSemanticsSize, procedureFeatures.genSemanticsSize);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, binaryId, hardHash, startRVA, isLibrary, isThunk, peSegment, procName, apiCalls, code, codeSize, genCode, genCodeSize, semantics, semanticsSize, genSemantics, genSemanticsSize);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProcedureFeatures {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    binaryId: ").append(toIndentedString(binaryId)).append("\n");
    sb.append("    hardHash: ").append(toIndentedString(hardHash)).append("\n");
    sb.append("    startRVA: ").append(toIndentedString(startRVA)).append("\n");
    sb.append("    isLibrary: ").append(toIndentedString(isLibrary)).append("\n");
    sb.append("    isThunk: ").append(toIndentedString(isThunk)).append("\n");
    sb.append("    peSegment: ").append(toIndentedString(peSegment)).append("\n");
    sb.append("    procName: ").append(toIndentedString(procName)).append("\n");
    sb.append("    apiCalls: ").append(toIndentedString(apiCalls)).append("\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    codeSize: ").append(toIndentedString(codeSize)).append("\n");
    sb.append("    genCode: ").append(toIndentedString(genCode)).append("\n");
    sb.append("    genCodeSize: ").append(toIndentedString(genCodeSize)).append("\n");
    sb.append("    semantics: ").append(toIndentedString(semantics)).append("\n");
    sb.append("    semanticsSize: ").append(toIndentedString(semanticsSize)).append("\n");
    sb.append("    genSemantics: ").append(toIndentedString(genSemantics)).append("\n");
    sb.append("    genSemanticsSize: ").append(toIndentedString(genSemanticsSize)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

