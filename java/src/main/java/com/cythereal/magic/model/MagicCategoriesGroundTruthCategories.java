package com.cythereal.magic.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The category and score as the number of binaries with this label correlated to the query binary..
 */
@ApiModel(description = "The category and score as the number of binaries with this label correlated to the query binary..")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class MagicCategoriesGroundTruthCategories {
  @SerializedName("name")
  private String name = null;

  @SerializedName("score")
  private Integer score = null;

  public MagicCategoriesGroundTruthCategories name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(example = "ransomeware", value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public MagicCategoriesGroundTruthCategories score(Integer score) {
    this.score = score;
    return this;
  }

   /**
   * Get score
   * @return score
  **/
  @ApiModelProperty(example = "6", value = "")
  public Integer getScore() {
    return score;
  }

  public void setScore(Integer score) {
    this.score = score;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MagicCategoriesGroundTruthCategories magicCategoriesGroundTruthCategories = (MagicCategoriesGroundTruthCategories) o;
    return Objects.equals(this.name, magicCategoriesGroundTruthCategories.name) &&
        Objects.equals(this.score, magicCategoriesGroundTruthCategories.score);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, score);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MagicCategoriesGroundTruthCategories {\n");
    
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    score: ").append(toIndentedString(score)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

