package com.cythereal.magic.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Information about the child of a file. Contains the meta-data about the file.
 */
@ApiModel(description = "Information about the child of a file. Contains the meta-data about the file.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class ChildFileInfo {
  @SerializedName("service_name")
  private String serviceName = null;

  /**
   * Status of the analysis that created the child file.
   */
  public enum StatusEnum {
    @SerializedName("success")
    SUCCESS("success"),
    
    @SerializedName("failed")
    FAILED("failed"),
    
    @SerializedName("partial")
    PARTIAL("partial");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
  }

  @SerializedName("status")
  private StatusEnum status = null;

  @SerializedName("child")
  private String child = null;

  public ChildFileInfo serviceName(String serviceName) {
    this.serviceName = serviceName;
    return this;
  }

   /**
   * Name of the analysis that created the child file.
   * @return serviceName
  **/
  @ApiModelProperty(required = true, value = "Name of the analysis that created the child file.")
  public String getServiceName() {
    return serviceName;
  }

  public void setServiceName(String serviceName) {
    this.serviceName = serviceName;
  }

  public ChildFileInfo status(StatusEnum status) {
    this.status = status;
    return this;
  }

   /**
   * Status of the analysis that created the child file.
   * @return status
  **/
  @ApiModelProperty(required = true, value = "Status of the analysis that created the child file.")
  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public ChildFileInfo child(String child) {
    this.child = child;
    return this;
  }

   /**
   * SHA1 of the child file.
   * @return child
  **/
  @ApiModelProperty(value = "SHA1 of the child file.")
  public String getChild() {
    return child;
  }

  public void setChild(String child) {
    this.child = child;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ChildFileInfo childFileInfo = (ChildFileInfo) o;
    return Objects.equals(this.serviceName, childFileInfo.serviceName) &&
        Objects.equals(this.status, childFileInfo.status) &&
        Objects.equals(this.child, childFileInfo.child);
  }

  @Override
  public int hashCode() {
    return Objects.hash(serviceName, status, child);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ChildFileInfo {\n");
    
    sb.append("    serviceName: ").append(toIndentedString(serviceName)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    child: ").append(toIndentedString(child)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

