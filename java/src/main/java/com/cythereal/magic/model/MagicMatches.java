package com.cythereal.magic.model;

import java.util.Objects;
import com.cythereal.magic.model.MagicMatchesDetails;
import com.cythereal.magic.model.MagicMatchesQuery;
import com.cythereal.magic.model.MagicMatchesSummary;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * The object containing binaries correlated with a query binary. Contains the MAGIC matches for a given binary.
 */
@ApiModel(description = "The object containing binaries correlated with a query binary. Contains the MAGIC matches for a given binary.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class MagicMatches {
  @SerializedName("version")
  private String version = null;

  @SerializedName("query")
  private MagicMatchesQuery query = null;

  @SerializedName("summary")
  private MagicMatchesSummary summary = null;

  @SerializedName("details")
  private List<MagicMatchesDetails> details = new ArrayList<MagicMatchesDetails>();

  public MagicMatches version(String version) {
    this.version = version;
    return this;
  }

   /**
   * Version of the MAGIC report. This is different from the API version.
   * @return version
  **/
  @ApiModelProperty(example = "1.0.0", required = true, value = "Version of the MAGIC report. This is different from the API version.")
  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public MagicMatches query(MagicMatchesQuery query) {
    this.query = query;
    return this;
  }

   /**
   * Get query
   * @return query
  **/
  @ApiModelProperty(value = "")
  public MagicMatchesQuery getQuery() {
    return query;
  }

  public void setQuery(MagicMatchesQuery query) {
    this.query = query;
  }

  public MagicMatches summary(MagicMatchesSummary summary) {
    this.summary = summary;
    return this;
  }

   /**
   * Get summary
   * @return summary
  **/
  @ApiModelProperty(value = "")
  public MagicMatchesSummary getSummary() {
    return summary;
  }

  public void setSummary(MagicMatchesSummary summary) {
    this.summary = summary;
  }

  public MagicMatches details(List<MagicMatchesDetails> details) {
    this.details = details;
    return this;
  }

  public MagicMatches addDetailsItem(MagicMatchesDetails detailsItem) {
    this.details.add(detailsItem);
    return this;
  }

   /**
   * Contains details about the similarity between the query binary and the MAGIC matches.
   * @return details
  **/
  @ApiModelProperty(example = "[{&quot;sha1&quot;:&quot;01ab20c6bb66f5e393f53dd7f54bd2d829745a6e&quot;,&quot;match_type&quot;:&quot;similar_packer_similar_payload&quot;,&quot;match_subtypes&quot;:[{&quot;match_subtype&quot;:&quot;matches_payload&quot;,&quot;similarity&quot;:1.0},{&quot;match_subtype&quot;:&quot;matches_original&quot;,&quot;similarity&quot;:1.0}],&quot;max_similarity&quot;:1.0},{&quot;sha1&quot;:&quot;008019b0ebf88cbcc4feab0347e3e823fdb1a372&quot;,&quot;match_type&quot;:&quot;similar_packer_different_payload&quot;,&quot;match_subtypes&quot;:[{&quot;match_subtype&quot;:&quot;matches_original&quot;,&quot;similarity&quot;:0.8868}],&quot;max_similarity&quot;:0.8868},{&quot;sha1&quot;:&quot;fcf5100bc222fc21b7326bb9a99d1aeac83a0f3e&quot;,&quot;match_type&quot;:&quot;similar_packer_different_payload&quot;,&quot;match_subtypes&quot;:[{&quot;match_subtype&quot;:&quot;matches_original&quot;,&quot;similarity&quot;:0.8868}],&quot;max_similarity&quot;:0.8868},{&quot;sha1&quot;:&quot;67120136e48d1307470e22c3a26d3a94bb843d36&quot;,&quot;match_type&quot;:&quot;different_packer_similar_payload&quot;,&quot;match_subtypes&quot;:[{&quot;match_subtype&quot;:&quot;matches_payload&quot;,&quot;similarity&quot;:1.0}],&quot;max_similarity&quot;:1.0},{&quot;sha1&quot;:&quot;0f5c62c1032beb536aa549930e37a83ade94a867&quot;,&quot;match_type&quot;:&quot;weak_similar&quot;,&quot;match_subtypes&quot;:[{&quot;match_subtype&quot;:&quot;matches_weak_down&quot;,&quot;similarity&quot;:0.8877},{&quot;match_subtype&quot;:&quot;matches_original&quot;,&quot;similarity&quot;:0.8877}],&quot;max_similarity&quot;:0.8877}]", value = "Contains details about the similarity between the query binary and the MAGIC matches.")
  public List<MagicMatchesDetails> getDetails() {
    return details;
  }

  public void setDetails(List<MagicMatchesDetails> details) {
    this.details = details;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MagicMatches magicMatches = (MagicMatches) o;
    return Objects.equals(this.version, magicMatches.version) &&
        Objects.equals(this.query, magicMatches.query) &&
        Objects.equals(this.summary, magicMatches.summary) &&
        Objects.equals(this.details, magicMatches.details);
  }

  @Override
  public int hashCode() {
    return Objects.hash(version, query, summary, details);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MagicMatches {\n");
    
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    query: ").append(toIndentedString(query)).append("\n");
    sb.append("    summary: ").append(toIndentedString(summary)).append("\n");
    sb.append("    details: ").append(toIndentedString(details)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

