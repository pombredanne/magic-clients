package com.cythereal.magic.model;

import java.util.Objects;
import com.cythereal.magic.model.ChildFileInfo;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;

/**
 * Information about a file that has been uploaded to MAGIC. Contain meta-data about the file as well as meta-data about processes that have been performed on the file.
 */
@ApiModel(description = "Information about a file that has been uploaded to MAGIC. Contain meta-data about the file as well as meta-data about processes that have been performed on the file.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class FileInfo {
  @SerializedName("sha1")
  private String sha1 = null;

  @SerializedName("md5")
  private String md5 = null;

  @SerializedName("unix_filetype")
  private String unixFiletype = null;

  @SerializedName("filepath")
  private List<String> filepath = new ArrayList<String>();

  @SerializedName("length")
  private Integer length = null;

  @SerializedName("parents")
  private List<String> parents = new ArrayList<String>();

  @SerializedName("uploadDate")
  private DateTime uploadDate = null;

  @SerializedName("object_class")
  private String objectClass = null;

  @SerializedName("children")
  private List<ChildFileInfo> children = new ArrayList<ChildFileInfo>();

  public FileInfo sha1(String sha1) {
    this.sha1 = sha1;
    return this;
  }

   /**
   * Get sha1
   * @return sha1
  **/
  @ApiModelProperty(required = true, value = "")
  public String getSha1() {
    return sha1;
  }

  public void setSha1(String sha1) {
    this.sha1 = sha1;
  }

  public FileInfo md5(String md5) {
    this.md5 = md5;
    return this;
  }

   /**
   * Get md5
   * @return md5
  **/
  @ApiModelProperty(required = true, value = "")
  public String getMd5() {
    return md5;
  }

  public void setMd5(String md5) {
    this.md5 = md5;
  }

  public FileInfo unixFiletype(String unixFiletype) {
    this.unixFiletype = unixFiletype;
    return this;
  }

   /**
   * Output of the unix `file` command for the file.
   * @return unixFiletype
  **/
  @ApiModelProperty(value = "Output of the unix `file` command for the file.")
  public String getUnixFiletype() {
    return unixFiletype;
  }

  public void setUnixFiletype(String unixFiletype) {
    this.unixFiletype = unixFiletype;
  }

  public FileInfo filepath(List<String> filepath) {
    this.filepath = filepath;
    return this;
  }

  public FileInfo addFilepathItem(String filepathItem) {
    this.filepath.add(filepathItem);
    return this;
  }

   /**
   * The original file paths associated with the file.
   * @return filepath
  **/
  @ApiModelProperty(value = "The original file paths associated with the file.")
  public List<String> getFilepath() {
    return filepath;
  }

  public void setFilepath(List<String> filepath) {
    this.filepath = filepath;
  }

  public FileInfo length(Integer length) {
    this.length = length;
    return this;
  }

   /**
   * Size of the file.
   * @return length
  **/
  @ApiModelProperty(value = "Size of the file.")
  public Integer getLength() {
    return length;
  }

  public void setLength(Integer length) {
    this.length = length;
  }

  public FileInfo parents(List<String> parents) {
    this.parents = parents;
    return this;
  }

  public FileInfo addParentsItem(String parentsItem) {
    this.parents.add(parentsItem);
    return this;
  }

   /**
   * The SHA1 of the file(s) that this file was generated from.
   * @return parents
  **/
  @ApiModelProperty(value = "The SHA1 of the file(s) that this file was generated from.")
  public List<String> getParents() {
    return parents;
  }

  public void setParents(List<String> parents) {
    this.parents = parents;
  }

  public FileInfo uploadDate(DateTime uploadDate) {
    this.uploadDate = uploadDate;
    return this;
  }

   /**
   * The timestamp of the original upload
   * @return uploadDate
  **/
  @ApiModelProperty(value = "The timestamp of the original upload")
  public DateTime getUploadDate() {
    return uploadDate;
  }

  public void setUploadDate(DateTime uploadDate) {
    this.uploadDate = uploadDate;
  }

  public FileInfo objectClass(String objectClass) {
    this.objectClass = objectClass;
    return this;
  }

   /**
   * An internal indicator of the type of object this file is.
   * @return objectClass
  **/
  @ApiModelProperty(required = true, value = "An internal indicator of the type of object this file is.")
  public String getObjectClass() {
    return objectClass;
  }

  public void setObjectClass(String objectClass) {
    this.objectClass = objectClass;
  }

  public FileInfo children(List<ChildFileInfo> children) {
    this.children = children;
    return this;
  }

  public FileInfo addChildrenItem(ChildFileInfo childrenItem) {
    this.children.add(childrenItem);
    return this;
  }

   /**
   * The list of files that were created from this file.
   * @return children
  **/
  @ApiModelProperty(required = true, value = "The list of files that were created from this file.")
  public List<ChildFileInfo> getChildren() {
    return children;
  }

  public void setChildren(List<ChildFileInfo> children) {
    this.children = children;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FileInfo fileInfo = (FileInfo) o;
    return Objects.equals(this.sha1, fileInfo.sha1) &&
        Objects.equals(this.md5, fileInfo.md5) &&
        Objects.equals(this.unixFiletype, fileInfo.unixFiletype) &&
        Objects.equals(this.filepath, fileInfo.filepath) &&
        Objects.equals(this.length, fileInfo.length) &&
        Objects.equals(this.parents, fileInfo.parents) &&
        Objects.equals(this.uploadDate, fileInfo.uploadDate) &&
        Objects.equals(this.objectClass, fileInfo.objectClass) &&
        Objects.equals(this.children, fileInfo.children);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sha1, md5, unixFiletype, filepath, length, parents, uploadDate, objectClass, children);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class FileInfo {\n");
    
    sb.append("    sha1: ").append(toIndentedString(sha1)).append("\n");
    sb.append("    md5: ").append(toIndentedString(md5)).append("\n");
    sb.append("    unixFiletype: ").append(toIndentedString(unixFiletype)).append("\n");
    sb.append("    filepath: ").append(toIndentedString(filepath)).append("\n");
    sb.append("    length: ").append(toIndentedString(length)).append("\n");
    sb.append("    parents: ").append(toIndentedString(parents)).append("\n");
    sb.append("    uploadDate: ").append(toIndentedString(uploadDate)).append("\n");
    sb.append("    objectClass: ").append(toIndentedString(objectClass)).append("\n");
    sb.append("    children: ").append(toIndentedString(children)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

