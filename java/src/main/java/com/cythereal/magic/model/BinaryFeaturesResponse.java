package com.cythereal.magic.model;

import java.util.Objects;
import com.cythereal.magic.model.ProcedureFeatures;
import com.cythereal.magic.model.ResponseObject;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * BinaryFeaturesResponse
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class BinaryFeaturesResponse extends ResponseObject {
  @SerializedName("answer")
  private Map<String, ProcedureFeatures> answer = new HashMap<String, ProcedureFeatures>();

  public BinaryFeaturesResponse answer(Map<String, ProcedureFeatures> answer) {
    this.answer = answer;
    return this;
  }

  public BinaryFeaturesResponse putAnswerItem(String key, ProcedureFeatures answerItem) {
    this.answer.put(key, answerItem);
    return this;
  }

   /**
   * A mapping object that maps the RVA of all procedures in the binary to the genomic features of that procedure.
   * @return answer
  **/
  @ApiModelProperty(value = "A mapping object that maps the RVA of all procedures in the binary to the genomic features of that procedure.")
  public Map<String, ProcedureFeatures> getAnswer() {
    return answer;
  }

  public void setAnswer(Map<String, ProcedureFeatures> answer) {
    this.answer = answer;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BinaryFeaturesResponse binaryFeaturesResponse = (BinaryFeaturesResponse) o;
    return Objects.equals(this.answer, binaryFeaturesResponse.answer) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(answer, super.hashCode());
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BinaryFeaturesResponse {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    answer: ").append(toIndentedString(answer)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

