package com.cythereal.magic.model;

import java.util.Objects;
import com.cythereal.magic.model.MagicMatchesDetailsMatchSubtypes;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * The similarity details of a magic match.
 */
@ApiModel(description = "The similarity details of a magic match.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class MagicMatchesDetails {
  @SerializedName("sha1")
  private String sha1 = null;

  /**
   * The MAGIC classification for the match.
   */
  public enum MatchTypeEnum {
    @SerializedName("similar_packer_similar_payload")
    SIMILAR_PACKER_SIMILAR_PAYLOAD("similar_packer_similar_payload"),
    
    @SerializedName("similar_packer_different_payload")
    SIMILAR_PACKER_DIFFERENT_PAYLOAD("similar_packer_different_payload"),
    
    @SerializedName("different_packer_similar_payload")
    DIFFERENT_PACKER_SIMILAR_PAYLOAD("different_packer_similar_payload"),
    
    @SerializedName("weak_similar")
    WEAK_SIMILAR("weak_similar");

    private String value;

    MatchTypeEnum(String value) {
      this.value = value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
  }

  @SerializedName("match_type")
  private MatchTypeEnum matchType = null;

  @SerializedName("match_subtypes")
  private List<MagicMatchesDetailsMatchSubtypes> matchSubtypes = new ArrayList<MagicMatchesDetailsMatchSubtypes>();

  @SerializedName("max_similarity")
  private Float maxSimilarity = null;

  public MagicMatchesDetails sha1(String sha1) {
    this.sha1 = sha1;
    return this;
  }

   /**
   * The SHA1 of the matched binary.
   * @return sha1
  **/
  @ApiModelProperty(example = "01ab20c6bb66f5e393f53dd7f54bd2d829745a6e", value = "The SHA1 of the matched binary.")
  public String getSha1() {
    return sha1;
  }

  public void setSha1(String sha1) {
    this.sha1 = sha1;
  }

  public MagicMatchesDetails matchType(MatchTypeEnum matchType) {
    this.matchType = matchType;
    return this;
  }

   /**
   * The MAGIC classification for the match.
   * @return matchType
  **/
  @ApiModelProperty(value = "The MAGIC classification for the match.")
  public MatchTypeEnum getMatchType() {
    return matchType;
  }

  public void setMatchType(MatchTypeEnum matchType) {
    this.matchType = matchType;
  }

  public MagicMatchesDetails matchSubtypes(List<MagicMatchesDetailsMatchSubtypes> matchSubtypes) {
    this.matchSubtypes = matchSubtypes;
    return this;
  }

  public MagicMatchesDetails addMatchSubtypesItem(MagicMatchesDetailsMatchSubtypes matchSubtypesItem) {
    this.matchSubtypes.add(matchSubtypesItem);
    return this;
  }

   /**
   * Finer grained identification of match types.
   * @return matchSubtypes
  **/
  @ApiModelProperty(value = "Finer grained identification of match types.")
  public List<MagicMatchesDetailsMatchSubtypes> getMatchSubtypes() {
    return matchSubtypes;
  }

  public void setMatchSubtypes(List<MagicMatchesDetailsMatchSubtypes> matchSubtypes) {
    this.matchSubtypes = matchSubtypes;
  }

  public MagicMatchesDetails maxSimilarity(Float maxSimilarity) {
    this.maxSimilarity = maxSimilarity;
    return this;
  }

   /**
   * The maximum similarity value from `match_subtypes`.
   * @return maxSimilarity
  **/
  @ApiModelProperty(example = "0.85", value = "The maximum similarity value from `match_subtypes`.")
  public Float getMaxSimilarity() {
    return maxSimilarity;
  }

  public void setMaxSimilarity(Float maxSimilarity) {
    this.maxSimilarity = maxSimilarity;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MagicMatchesDetails magicMatchesDetails = (MagicMatchesDetails) o;
    return Objects.equals(this.sha1, magicMatchesDetails.sha1) &&
        Objects.equals(this.matchType, magicMatchesDetails.matchType) &&
        Objects.equals(this.matchSubtypes, magicMatchesDetails.matchSubtypes) &&
        Objects.equals(this.maxSimilarity, magicMatchesDetails.maxSimilarity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sha1, matchType, matchSubtypes, maxSimilarity);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MagicMatchesDetails {\n");
    
    sb.append("    sha1: ").append(toIndentedString(sha1)).append("\n");
    sb.append("    matchType: ").append(toIndentedString(matchType)).append("\n");
    sb.append("    matchSubtypes: ").append(toIndentedString(matchSubtypes)).append("\n");
    sb.append("    maxSimilarity: ").append(toIndentedString(maxSimilarity)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

