package com.cythereal.magic.model;

import java.util.Objects;
import com.cythereal.magic.model.MagicCategoriesGroundTruthCategories;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * The categories directly related to this binary.
 */
@ApiModel(description = "The categories directly related to this binary.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class MagicCategoriesGroundTruth {
  @SerializedName("total_categories")
  private Integer totalCategories = null;

  @SerializedName("categories")
  private List<MagicCategoriesGroundTruthCategories> categories = new ArrayList<MagicCategoriesGroundTruthCategories>();

  public MagicCategoriesGroundTruth totalCategories(Integer totalCategories) {
    this.totalCategories = totalCategories;
    return this;
  }

   /**
   * Get totalCategories
   * @return totalCategories
  **/
  @ApiModelProperty(example = "1", value = "")
  public Integer getTotalCategories() {
    return totalCategories;
  }

  public void setTotalCategories(Integer totalCategories) {
    this.totalCategories = totalCategories;
  }

  public MagicCategoriesGroundTruth categories(List<MagicCategoriesGroundTruthCategories> categories) {
    this.categories = categories;
    return this;
  }

  public MagicCategoriesGroundTruth addCategoriesItem(MagicCategoriesGroundTruthCategories categoriesItem) {
    this.categories.add(categoriesItem);
    return this;
  }

   /**
   * Get categories
   * @return categories
  **/
  @ApiModelProperty(value = "")
  public List<MagicCategoriesGroundTruthCategories> getCategories() {
    return categories;
  }

  public void setCategories(List<MagicCategoriesGroundTruthCategories> categories) {
    this.categories = categories;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MagicCategoriesGroundTruth magicCategoriesGroundTruth = (MagicCategoriesGroundTruth) o;
    return Objects.equals(this.totalCategories, magicCategoriesGroundTruth.totalCategories) &&
        Objects.equals(this.categories, magicCategoriesGroundTruth.categories);
  }

  @Override
  public int hashCode() {
    return Objects.hash(totalCategories, categories);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MagicCategoriesGroundTruth {\n");
    
    sb.append("    totalCategories: ").append(toIndentedString(totalCategories)).append("\n");
    sb.append("    categories: ").append(toIndentedString(categories)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

