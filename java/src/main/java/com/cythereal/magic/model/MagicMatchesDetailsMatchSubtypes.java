package com.cythereal.magic.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * MagicMatchesDetailsMatchSubtypes
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2017-05-11T01:50:30.991Z")
public class MagicMatchesDetailsMatchSubtypes {
  /**
   * Gets or Sets matchSubtype
   */
  public enum MatchSubtypeEnum {
    @SerializedName("matches_payload")
    PAYLOAD("matches_payload"),
    
    @SerializedName("matches_original")
    ORIGINAL("matches_original"),
    
    @SerializedName("matches_weak_up")
    WEAK_UP("matches_weak_up"),
    
    @SerializedName("matches_weak_down")
    WEAK_DOWN("matches_weak_down"),
    
    @SerializedName("matches_self")
    SELF("matches_self");

    private String value;

    MatchSubtypeEnum(String value) {
      this.value = value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }
  }

  @SerializedName("match_subtype")
  private MatchSubtypeEnum matchSubtype = null;

  @SerializedName("similarity")
  private Float similarity = null;

  public MagicMatchesDetailsMatchSubtypes matchSubtype(MatchSubtypeEnum matchSubtype) {
    this.matchSubtype = matchSubtype;
    return this;
  }

   /**
   * Get matchSubtype
   * @return matchSubtype
  **/
  @ApiModelProperty(value = "")
  public MatchSubtypeEnum getMatchSubtype() {
    return matchSubtype;
  }

  public void setMatchSubtype(MatchSubtypeEnum matchSubtype) {
    this.matchSubtype = matchSubtype;
  }

  public MagicMatchesDetailsMatchSubtypes similarity(Float similarity) {
    this.similarity = similarity;
    return this;
  }

   /**
   * The similarity score
   * @return similarity
  **/
  @ApiModelProperty(example = "0.85", value = "The similarity score")
  public Float getSimilarity() {
    return similarity;
  }

  public void setSimilarity(Float similarity) {
    this.similarity = similarity;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MagicMatchesDetailsMatchSubtypes magicMatchesDetailsMatchSubtypes = (MagicMatchesDetailsMatchSubtypes) o;
    return Objects.equals(this.matchSubtype, magicMatchesDetailsMatchSubtypes.matchSubtype) &&
        Objects.equals(this.similarity, magicMatchesDetailsMatchSubtypes.similarity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(matchSubtype, similarity);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class MagicMatchesDetailsMatchSubtypes {\n");
    
    sb.append("    matchSubtype: ").append(toIndentedString(matchSubtype)).append("\n");
    sb.append("    similarity: ").append(toIndentedString(similarity)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
  
}

