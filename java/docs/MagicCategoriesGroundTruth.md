
# MagicCategoriesGroundTruth

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalCategories** | **Integer** |  |  [optional]
**categories** | [**List&lt;MagicCategoriesGroundTruthCategories&gt;**](MagicCategoriesGroundTruthCategories.md) |  |  [optional]



