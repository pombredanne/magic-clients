
# ProcedureSimilarity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**semanticallyEquivalentProcedures** | [**List&lt;ProcedureSimilaritySemanticallyEquivalentProcedures&gt;**](ProcedureSimilaritySemanticallyEquivalentProcedures.md) | List of procedures with the exact semantics as the given procedure. |  [optional]



