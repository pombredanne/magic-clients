
# MagicMatchesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**answer** | [**MagicMatches**](MagicMatches.md) |  |  [optional]



