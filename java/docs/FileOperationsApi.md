# FileOperationsApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**download**](FileOperationsApi.md#download) | **GET** /download/{api_key}/{file_hash} | Download a file
[**query**](FileOperationsApi.md#query) | **GET** /query/{api_key}/{file_hash} | Query file info and analysis status
[**reprocess**](FileOperationsApi.md#reprocess) | **GET** /reprocess/{api_key}/{file_hash} | Reprocess a previously uploaded file
[**upload**](FileOperationsApi.md#upload) | **POST** /upload/{api_key} | Upload file for processing


<a name="download"></a>
# **download**
> File download(apiKey, fileHash)

Download a file

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import com.cythereal.magic.api.FileOperationsApi;


FileOperationsApi apiInstance = new FileOperationsApi();
String apiKey = "apiKey_example"; // String | API Key for accessing the service
String fileHash = "fileHash_example"; // String | A cryptographic hash of a file. Supported hashes are SHA1.
try {
    File result = apiInstance.download(apiKey, fileHash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FileOperationsApi#download");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**| API Key for accessing the service |
 **fileHash** | **String**| A cryptographic hash of a file. Supported hashes are SHA1. |

### Return type

[**File**](File.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="query"></a>
# **query**
> QueryResponse query(apiKey, fileHash)

Query file info and analysis status

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import com.cythereal.magic.api.FileOperationsApi;


FileOperationsApi apiInstance = new FileOperationsApi();
String apiKey = "apiKey_example"; // String | API Key for accessing the service
String fileHash = "fileHash_example"; // String | A cryptographic hash of a file. Supported hashes are SHA1.
try {
    QueryResponse result = apiInstance.query(apiKey, fileHash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FileOperationsApi#query");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**| API Key for accessing the service |
 **fileHash** | **String**| A cryptographic hash of a file. Supported hashes are SHA1. |

### Return type

[**QueryResponse**](QueryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="reprocess"></a>
# **reprocess**
> ResponseObject reprocess(apiKey, fileHash, priority)

Reprocess a previously uploaded file

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import com.cythereal.magic.api.FileOperationsApi;


FileOperationsApi apiInstance = new FileOperationsApi();
String apiKey = "apiKey_example"; // String | API Key for accessing the service
String fileHash = "fileHash_example"; // String | A cryptographic hash of a file. Supported hashes are SHA1.
Integer priority = 2; // Integer | Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges.
try {
    ResponseObject result = apiInstance.reprocess(apiKey, fileHash, priority);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FileOperationsApi#reprocess");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**| API Key for accessing the service |
 **fileHash** | **String**| A cryptographic hash of a file. Supported hashes are SHA1. |
 **priority** | **Integer**| Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. | [optional] [default to 2]

### Return type

[**ResponseObject**](ResponseObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="upload"></a>
# **upload**
> ResponseObject upload(apiKey, filedata, origFilePath, password, priority, force)

Upload file for processing

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import com.cythereal.magic.api.FileOperationsApi;


FileOperationsApi apiInstance = new FileOperationsApi();
String apiKey = "apiKey_example"; // String | API Key for accessing the service
File filedata = new File("/path/to/file.txt"); // File | Binary contents of the file
String origFilePath = "origFilePath_example"; // String | Name of the file being uploaded
String password = "password_example"; // String | If uploading a password protected zip, this field MUST contain the password.
Integer priority = 2; // Integer | Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges.
Boolean force = false; // Boolean | If true, will force existing file to be reanalyzed.
try {
    ResponseObject result = apiInstance.upload(apiKey, filedata, origFilePath, password, priority, force);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FileOperationsApi#upload");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**| API Key for accessing the service |
 **filedata** | **File**| Binary contents of the file |
 **origFilePath** | **String**| Name of the file being uploaded | [optional]
 **password** | **String**| If uploading a password protected zip, this field MUST contain the password. | [optional]
 **priority** | **Integer**| Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. | [optional] [default to 2]
 **force** | **Boolean**| If true, will force existing file to be reanalyzed. | [optional] [default to false]

### Return type

[**ResponseObject**](ResponseObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

