
# MagicMatchesSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**similarPackerSimilarPayload** | **List&lt;String&gt;** |  |  [optional]
**similarPackerDifferentPayload** | **List&lt;String&gt;** |  |  [optional]
**differentPackerSimilarPayload** | **List&lt;String&gt;** |  |  [optional]
**weakSimilar** | **List&lt;String&gt;** |  |  [optional]



