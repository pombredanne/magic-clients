
# MagicMatchesDetailsMatchSubtypes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**matchSubtype** | [**MatchSubtypeEnum**](#MatchSubtypeEnum) |  |  [optional]
**similarity** | **Float** | The similarity score |  [optional]


<a name="MatchSubtypeEnum"></a>
## Enum: MatchSubtypeEnum
Name | Value
---- | -----
PAYLOAD | &quot;matches_payload&quot;
ORIGINAL | &quot;matches_original&quot;
WEAK_UP | &quot;matches_weak_up&quot;
WEAK_DOWN | &quot;matches_weak_down&quot;
SELF | &quot;matches_self&quot;



