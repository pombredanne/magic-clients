
# MagicCategoriesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**answer** | [**MagicCategories**](MagicCategories.md) |  |  [optional]



