
# MagicCategories

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groundTruth** | [**MagicCategoriesGroundTruth**](MagicCategoriesGroundTruth.md) |  |  [optional]
**categorizationResult** | [**MagicCategoriesCategorizationResult**](MagicCategoriesCategorizationResult.md) |  |  [optional]



