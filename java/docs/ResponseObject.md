
# ResponseObject

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | [**StatusEnum**](#StatusEnum) | Indicates whether an operation was successful or not. | 
**statuscode** | **Integer** | Status code for the operation. This is NOT the same as an HTTP status code. It is closer to a unix exit code. Zero indicates success. Non-zero indicates failure. | 
**vbVersion** | **String** |  | 
**message** | **String** | A message for the user. Current version of the API. |  [optional]
**hash** | **String** | The hash value used in the query. |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
SUCCESS | &quot;success&quot;
FAILURE | &quot;failure&quot;



