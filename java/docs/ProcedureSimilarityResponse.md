
# ProcedureSimilarityResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**answer** | [**ProcedureSimilarity**](ProcedureSimilarity.md) |  |  [optional]



