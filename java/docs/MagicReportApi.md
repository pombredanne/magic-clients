# MagicReportApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**magicCategories**](MagicReportApi.md#magicCategories) | **GET** /categories/{api_key}/{file_hash} | Retrieve the MAGIC categories for a binary. **Alpha Level Feature**
[**magicCorrelations**](MagicReportApi.md#magicCorrelations) | **GET** /magic/{api_key}/{file_hash} | Retrieve the MAGIC correlations for a binary.
[**putMagicCategories**](MagicReportApi.md#putMagicCategories) | **PUT** /categories/{api_key}/{file_hash} | Save category for a binary. **Alpha Level Feature**


<a name="magicCategories"></a>
# **magicCategories**
> MagicCategoriesResponse magicCategories(apiKey, fileHash)

Retrieve the MAGIC categories for a binary. **Alpha Level Feature**

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import com.cythereal.magic.api.MagicReportApi;


MagicReportApi apiInstance = new MagicReportApi();
String apiKey = "apiKey_example"; // String | API Key for accessing the service
String fileHash = "fileHash_example"; // String | A cryptographic hash of a file. Supported hashes are SHA1.
try {
    MagicCategoriesResponse result = apiInstance.magicCategories(apiKey, fileHash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MagicReportApi#magicCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**| API Key for accessing the service |
 **fileHash** | **String**| A cryptographic hash of a file. Supported hashes are SHA1. |

### Return type

[**MagicCategoriesResponse**](MagicCategoriesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="magicCorrelations"></a>
# **magicCorrelations**
> MagicMatchesResponse magicCorrelations(apiKey, fileHash, details, threshold)

Retrieve the MAGIC correlations for a binary.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import com.cythereal.magic.api.MagicReportApi;


MagicReportApi apiInstance = new MagicReportApi();
String apiKey = "apiKey_example"; // String | API Key for accessing the service
String fileHash = "fileHash_example"; // String | A cryptographic hash of a file. Supported hashes are SHA1.
Boolean details = true; // Boolean | If false, omits the match details. Defaults to true.
Float threshold = 0.7F; // Float | Only similarity matches at value equal or above threshold will be considered. MUST be in range [0, 1]. Defaults to 0.7.
try {
    MagicMatchesResponse result = apiInstance.magicCorrelations(apiKey, fileHash, details, threshold);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MagicReportApi#magicCorrelations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**| API Key for accessing the service |
 **fileHash** | **String**| A cryptographic hash of a file. Supported hashes are SHA1. |
 **details** | **Boolean**| If false, omits the match details. Defaults to true. | [optional] [default to true]
 **threshold** | **Float**| Only similarity matches at value equal or above threshold will be considered. MUST be in range [0, 1]. Defaults to 0.7. | [optional] [default to 0.7]

### Return type

[**MagicMatchesResponse**](MagicMatchesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="putMagicCategories"></a>
# **putMagicCategories**
> ResponseObject putMagicCategories(apiKey, fileHash, category)

Save category for a binary. **Alpha Level Feature**

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import com.cythereal.magic.api.MagicReportApi;


MagicReportApi apiInstance = new MagicReportApi();
String apiKey = "apiKey_example"; // String | API Key for accessing the service
String fileHash = "fileHash_example"; // String | A cryptographic hash of a file. Supported hashes are SHA1.
String category = "category_example"; // String | Category assigned to the file.
try {
    ResponseObject result = apiInstance.putMagicCategories(apiKey, fileHash, category);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MagicReportApi#putMagicCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**| API Key for accessing the service |
 **fileHash** | **String**| A cryptographic hash of a file. Supported hashes are SHA1. |
 **category** | **String**| Category assigned to the file. |

### Return type

[**ResponseObject**](ResponseObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

