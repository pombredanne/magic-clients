
# ProcedureFeatures

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | The ID of the function. It takes the form \&quot;&#x60;binary_id&#x60;/&#x60;startRVA&#x60;\&quot;. |  [optional]
**binaryId** | **String** | The SHA1 of the binary this function is in. |  [optional]
**hardHash** | **String** | This is a special hash created by MAGIC to identify semantically equivalent functions. If two functions have an equivalent hardHash, then the semantics of both functions are the same, i.e. the functions can be considered functionally equivalent. |  [optional]
**startRVA** | **String** | The relative virtual address of the function |  [optional]
**isLibrary** | **Boolean** | Indicates  if this function was identified as a library function. |  [optional]
**isThunk** | **Boolean** | Indicates if this function was identified as a thunk (jump) function. |  [optional]
**peSegment** | **String** | Lists the [PE Segment](https://msdn.microsoft.com/en-us/library/ms809762.aspx) that the function was located in. |  [optional]
**procName** | **String** | The name of the procedure. If debug symbols were not present in the original file, this will be of the form &#x60;sub_xxxxxx&#x60; where &#x60;xxxxxx&#x60; is the virtual address of the function. |  [optional]
**apiCalls** | **List&lt;String&gt;** | The list of APIs called from this function. |  [optional]
**code** | [**List&lt;List&lt;String&gt;&gt;**](List.md) | The disassembly of this function. This is a list of lists where each sublist represents a disassembly block. Each block contains a list of the instructions that compose the block. |  [optional]
**codeSize** | **Integer** | The number of instructions in the disassembly. |  [optional]
**genCode** | [**List&lt;List&lt;String&gt;&gt;**](List.md) | Generalized disassembly. This is the same as &#x60;code&#x60;, but with the operands in code abstracted to variables instead of register names. |  [optional]
**genCodeSize** | **Integer** | The number of generalized instructions in &#x60;gen_code&#x60;. |  [optional]
**semantics** | [**List&lt;List&lt;String&gt;&gt;**](List.md) | The semantics of the instructions in &#x60;code&#x60;. This is a list of lists where each inner list corresponds to a block from &#x60;code&#x60;. This inner list contains the effect that executing the represented block will have on registers and memory locations. |  [optional]
**semanticsSize** | **Integer** | The number of individual semantics statements in &#x60;semantics&#x60; |  [optional]
**genSemantics** | [**List&lt;List&lt;String&gt;&gt;**](List.md) | Generalized semantics. This is the same as &#x60;semantics&#x60; but with registers and memory locations abstracted to variables. |  [optional]
**genSemanticsSize** | **Integer** | The number of individual semantics statements in &#x60;gen_semantics&#x60; |  [optional]



