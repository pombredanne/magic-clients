
# ProcedureFeaturesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**answer** | [**ProcedureFeatures**](ProcedureFeatures.md) |  |  [optional]



