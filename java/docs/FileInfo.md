
# FileInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sha1** | **String** |  | 
**md5** | **String** |  | 
**unixFiletype** | **String** | Output of the unix &#x60;file&#x60; command for the file. |  [optional]
**filepath** | **List&lt;String&gt;** | The original file paths associated with the file. |  [optional]
**length** | **Integer** | Size of the file. |  [optional]
**parents** | **List&lt;String&gt;** | The SHA1 of the file(s) that this file was generated from. |  [optional]
**uploadDate** | [**DateTime**](DateTime.md) | The timestamp of the original upload |  [optional]
**objectClass** | **String** | An internal indicator of the type of object this file is. | 
**children** | [**List&lt;ChildFileInfo&gt;**](ChildFileInfo.md) | The list of files that were created from this file. | 



