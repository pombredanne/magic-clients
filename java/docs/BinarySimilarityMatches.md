
# BinarySimilarityMatches

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fileHash** | **String** | SHA1 of a similar binary |  [optional]
**similarity** | **Float** | Similarity score of the matched binary |  [optional]



