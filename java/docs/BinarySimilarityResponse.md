
# BinarySimilarityResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**answer** | [**BinarySimilarity**](BinarySimilarity.md) |  |  [optional]



