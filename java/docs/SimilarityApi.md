# SimilarityApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**similarBinaries**](SimilarityApi.md#similarBinaries) | **GET** /search/binary/{api_key}/{file_hash} | Search for similar binaries.
[**similarProcedures**](SimilarityApi.md#similarProcedures) | **GET** /search/procs/{api_key}/{file_hash}/{proc_rva} | Search for procedures similar to a given procedure.


<a name="similarBinaries"></a>
# **similarBinaries**
> BinarySimilarityResponse similarBinaries(apiKey, fileHash, threshold, level)

Search for similar binaries.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import com.cythereal.magic.api.SimilarityApi;


SimilarityApi apiInstance = new SimilarityApi();
String apiKey = "apiKey_example"; // String | API Key for accessing the service
String fileHash = "fileHash_example"; // String | A cryptographic hash of a file. Supported hashes are SHA1.
Float threshold = 0.7F; // Float | Similarity threshold, default 0.7.
Integer level = 3; // Integer | Similarity Search Level from 1 through 5, default is 3.
try {
    BinarySimilarityResponse result = apiInstance.similarBinaries(apiKey, fileHash, threshold, level);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SimilarityApi#similarBinaries");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**| API Key for accessing the service |
 **fileHash** | **String**| A cryptographic hash of a file. Supported hashes are SHA1. |
 **threshold** | **Float**| Similarity threshold, default 0.7. | [optional] [default to 0.7]
 **level** | **Integer**| Similarity Search Level from 1 through 5, default is 3. | [optional] [default to 3]

### Return type

[**BinarySimilarityResponse**](BinarySimilarityResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="similarProcedures"></a>
# **similarProcedures**
> ProcedureSimilarityResponse similarProcedures(apiKey, fileHash, procRva)

Search for procedures similar to a given procedure.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import com.cythereal.magic.api.SimilarityApi;


SimilarityApi apiInstance = new SimilarityApi();
String apiKey = "apiKey_example"; // String | API Key for accessing the service
String fileHash = "fileHash_example"; // String | A cryptographic hash of a file. Supported hashes are SHA1.
String procRva = "procRva_example"; // String | The RVA of the procedure to find similarities with. Formatted 0xnnnn
try {
    ProcedureSimilarityResponse result = apiInstance.similarProcedures(apiKey, fileHash, procRva);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SimilarityApi#similarProcedures");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**| API Key for accessing the service |
 **fileHash** | **String**| A cryptographic hash of a file. Supported hashes are SHA1. |
 **procRva** | **String**| The RVA of the procedure to find similarities with. Formatted 0xnnnn |

### Return type

[**ProcedureSimilarityResponse**](ProcedureSimilarityResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

