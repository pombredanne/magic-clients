# UserOperationsApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**register**](UserOperationsApi.md#register) | **GET** /register | Register to receive a MAGIC API key.


<a name="register"></a>
# **register**
> ResponseObject register(email, name)

Register to receive a MAGIC API key.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import com.cythereal.magic.api.UserOperationsApi;


UserOperationsApi apiInstance = new UserOperationsApi();
String email = "email_example"; // String | Email Address
String name = "name_example"; // String | Full Name
try {
    ResponseObject result = apiInstance.register(email, name);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserOperationsApi#register");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| Email Address |
 **name** | **String**| Full Name |

### Return type

[**ResponseObject**](ResponseObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

