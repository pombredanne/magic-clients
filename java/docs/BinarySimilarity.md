
# BinarySimilarity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**matches** | [**List&lt;BinarySimilarityMatches&gt;**](BinarySimilarityMatches.md) | List of binaries similar to the query binary. Sorted by decreasing similarity. |  [optional]



