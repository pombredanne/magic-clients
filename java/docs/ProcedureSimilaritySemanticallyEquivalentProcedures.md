
# ProcedureSimilaritySemanticallyEquivalentProcedures

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**procName** | **String** | Name of the matched procedure. |  [optional]
**procId** | **String** | ID of the matched procedure. |  [optional]



