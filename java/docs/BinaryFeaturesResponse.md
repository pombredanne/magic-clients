
# BinaryFeaturesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**answer** | [**Map&lt;String, ProcedureFeatures&gt;**](ProcedureFeatures.md) | A mapping object that maps the RVA of all procedures in the binary to the genomic features of that procedure. |  [optional]



