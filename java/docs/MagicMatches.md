
# MagicMatches

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | **String** | Version of the MAGIC report. This is different from the API version. | 
**query** | [**MagicMatchesQuery**](MagicMatchesQuery.md) |  |  [optional]
**summary** | [**MagicMatchesSummary**](MagicMatchesSummary.md) |  |  [optional]
**details** | [**List&lt;MagicMatchesDetails&gt;**](MagicMatchesDetails.md) | Contains details about the similarity between the query binary and the MAGIC matches. |  [optional]



