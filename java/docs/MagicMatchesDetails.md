
# MagicMatchesDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sha1** | **String** | The SHA1 of the matched binary. |  [optional]
**matchType** | [**MatchTypeEnum**](#MatchTypeEnum) | The MAGIC classification for the match. |  [optional]
**matchSubtypes** | [**List&lt;MagicMatchesDetailsMatchSubtypes&gt;**](MagicMatchesDetailsMatchSubtypes.md) | Finer grained identification of match types. |  [optional]
**maxSimilarity** | **Float** | The maximum similarity value from &#x60;match_subtypes&#x60;. |  [optional]


<a name="MatchTypeEnum"></a>
## Enum: MatchTypeEnum
Name | Value
---- | -----
SIMILAR_PACKER_SIMILAR_PAYLOAD | &quot;similar_packer_similar_payload&quot;
SIMILAR_PACKER_DIFFERENT_PAYLOAD | &quot;similar_packer_different_payload&quot;
DIFFERENT_PACKER_SIMILAR_PAYLOAD | &quot;different_packer_similar_payload&quot;
WEAK_SIMILAR | &quot;weak_similar&quot;



