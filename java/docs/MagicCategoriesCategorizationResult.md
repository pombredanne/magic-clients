
# MagicCategoriesCategorizationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**totalCategories** | **Integer** |  |  [optional]
**categories** | [**List&lt;MagicCategoriesCategorizationResultCategories&gt;**](MagicCategoriesCategorizationResultCategories.md) |  |  [optional]



