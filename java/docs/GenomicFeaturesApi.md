# GenomicFeaturesApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**binaryFeatures**](GenomicFeaturesApi.md#binaryFeatures) | **GET** /show/binary/{api_key}/{file_hash} | Show the genomic features for a given binary.
[**procedureFeatures**](GenomicFeaturesApi.md#procedureFeatures) | **GET** /show/proc/{api_key}/{file_hash}/{proc_rva} | Show the genomic features for a given procedure.


<a name="binaryFeatures"></a>
# **binaryFeatures**
> BinaryFeaturesResponse binaryFeatures(apiKey, fileHash)

Show the genomic features for a given binary.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import com.cythereal.magic.api.GenomicFeaturesApi;


GenomicFeaturesApi apiInstance = new GenomicFeaturesApi();
String apiKey = "apiKey_example"; // String | API Key for accessing the service
String fileHash = "fileHash_example"; // String | A cryptographic hash of a file. Supported hashes are SHA1.
try {
    BinaryFeaturesResponse result = apiInstance.binaryFeatures(apiKey, fileHash);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GenomicFeaturesApi#binaryFeatures");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**| API Key for accessing the service |
 **fileHash** | **String**| A cryptographic hash of a file. Supported hashes are SHA1. |

### Return type

[**BinaryFeaturesResponse**](BinaryFeaturesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="procedureFeatures"></a>
# **procedureFeatures**
> ProcedureFeaturesResponse procedureFeatures(apiKey, fileHash, procRva)

Show the genomic features for a given procedure.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import com.cythereal.magic.api.GenomicFeaturesApi;


GenomicFeaturesApi apiInstance = new GenomicFeaturesApi();
String apiKey = "apiKey_example"; // String | API Key for accessing the service
String fileHash = "fileHash_example"; // String | A cryptographic hash of a file. Supported hashes are SHA1.
String procRva = "procRva_example"; // String | The RVA of the procedure to find similarities with. Formatted 0xnnnn
try {
    ProcedureFeaturesResponse result = apiInstance.procedureFeatures(apiKey, fileHash, procRva);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling GenomicFeaturesApi#procedureFeatures");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **String**| API Key for accessing the service |
 **fileHash** | **String**| A cryptographic hash of a file. Supported hashes are SHA1. |
 **procRva** | **String**| The RVA of the procedure to find similarities with. Formatted 0xnnnn |

### Return type

[**ProcedureFeaturesResponse**](ProcedureFeaturesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

