
# ChildFileInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**serviceName** | **String** | Name of the analysis that created the child file. | 
**status** | [**StatusEnum**](#StatusEnum) | Status of the analysis that created the child file. | 
**child** | **String** | SHA1 of the child file. |  [optional]


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----
SUCCESS | &quot;success&quot;
FAILED | &quot;failed&quot;
PARTIAL | &quot;partial&quot;



