
# QueryResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**answer** | [**FileInfo**](FileInfo.md) |  |  [optional]



