# swagger-java-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-java-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-java-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/swagger-java-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import io.swagger.client.*;
import io.swagger.client.auth.*;
import com.cythereal.magic.model.*;
import com.cythereal.magic.api.FileOperationsApi;

import java.io.File;
import java.util.*;

public class FileOperationsApiExample {

    public static void main(String[] args) {
        
        FileOperationsApi apiInstance = new FileOperationsApi();
        String apiKey = "apiKey_example"; // String | API Key for accessing the service
        String fileHash = "fileHash_example"; // String | A cryptographic hash of a file. Supported hashes are SHA1.
        try {
            File result = apiInstance.download(apiKey, fileHash);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling FileOperationsApi#download");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://api.magic.cythereal.com*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*FileOperationsApi* | [**download**](docs/FileOperationsApi.md#download) | **GET** /download/{api_key}/{file_hash} | Download a file
*FileOperationsApi* | [**query**](docs/FileOperationsApi.md#query) | **GET** /query/{api_key}/{file_hash} | Query file info and analysis status
*FileOperationsApi* | [**reprocess**](docs/FileOperationsApi.md#reprocess) | **GET** /reprocess/{api_key}/{file_hash} | Reprocess a previously uploaded file
*FileOperationsApi* | [**upload**](docs/FileOperationsApi.md#upload) | **POST** /upload/{api_key} | Upload file for processing
*GenomicFeaturesApi* | [**binaryFeatures**](docs/GenomicFeaturesApi.md#binaryFeatures) | **GET** /show/binary/{api_key}/{file_hash} | Show the genomic features for a given binary.
*GenomicFeaturesApi* | [**procedureFeatures**](docs/GenomicFeaturesApi.md#procedureFeatures) | **GET** /show/proc/{api_key}/{file_hash}/{proc_rva} | Show the genomic features for a given procedure.
*MagicReportApi* | [**magicCategories**](docs/MagicReportApi.md#magicCategories) | **GET** /categories/{api_key}/{file_hash} | Retrieve the MAGIC categories for a binary. **Alpha Level Feature**
*MagicReportApi* | [**magicCorrelations**](docs/MagicReportApi.md#magicCorrelations) | **GET** /magic/{api_key}/{file_hash} | Retrieve the MAGIC correlations for a binary.
*MagicReportApi* | [**putMagicCategories**](docs/MagicReportApi.md#putMagicCategories) | **PUT** /categories/{api_key}/{file_hash} | Save category for a binary. **Alpha Level Feature**
*SimilarityApi* | [**similarBinaries**](docs/SimilarityApi.md#similarBinaries) | **GET** /search/binary/{api_key}/{file_hash} | Search for similar binaries.
*SimilarityApi* | [**similarProcedures**](docs/SimilarityApi.md#similarProcedures) | **GET** /search/procs/{api_key}/{file_hash}/{proc_rva} | Search for procedures similar to a given procedure.
*UserOperationsApi* | [**register**](docs/UserOperationsApi.md#register) | **GET** /register | Register to receive a MAGIC API key.


## Documentation for Models

 - [BinaryFeaturesResponse](docs/BinaryFeaturesResponse.md)
 - [BinarySimilarity](docs/BinarySimilarity.md)
 - [BinarySimilarityMatches](docs/BinarySimilarityMatches.md)
 - [BinarySimilarityResponse](docs/BinarySimilarityResponse.md)
 - [ChildFileInfo](docs/ChildFileInfo.md)
 - [FileInfo](docs/FileInfo.md)
 - [MagicCategories](docs/MagicCategories.md)
 - [MagicCategoriesCategorizationResult](docs/MagicCategoriesCategorizationResult.md)
 - [MagicCategoriesCategorizationResultCategories](docs/MagicCategoriesCategorizationResultCategories.md)
 - [MagicCategoriesGroundTruth](docs/MagicCategoriesGroundTruth.md)
 - [MagicCategoriesGroundTruthCategories](docs/MagicCategoriesGroundTruthCategories.md)
 - [MagicCategoriesResponse](docs/MagicCategoriesResponse.md)
 - [MagicMatches](docs/MagicMatches.md)
 - [MagicMatchesDetails](docs/MagicMatchesDetails.md)
 - [MagicMatchesDetailsMatchSubtypes](docs/MagicMatchesDetailsMatchSubtypes.md)
 - [MagicMatchesQuery](docs/MagicMatchesQuery.md)
 - [MagicMatchesResponse](docs/MagicMatchesResponse.md)
 - [MagicMatchesSummary](docs/MagicMatchesSummary.md)
 - [ProcedureFeatures](docs/ProcedureFeatures.md)
 - [ProcedureFeaturesResponse](docs/ProcedureFeaturesResponse.md)
 - [ProcedureSimilarity](docs/ProcedureSimilarity.md)
 - [ProcedureSimilarityResponse](docs/ProcedureSimilarityResponse.md)
 - [ProcedureSimilaritySemanticallyEquivalentProcedures](docs/ProcedureSimilaritySemanticallyEquivalentProcedures.md)
 - [QueryResponse](docs/QueryResponse.md)
 - [ResponseObject](docs/ResponseObject.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author

support@cythereal.com

