Support: Email support@cythereal.com

# MAGIC API Clients

This repository holds the automatically generated MAGIC API clients. These clients are generated from the swagger spec of the MAGIC API using swagger codegen.  This code in this repository is automatically updated from swaggerhub.

The current swagger spec can be found at https://api.swaggerhub.com/apis/cyledoux/cythereal-magic-api/0.0.0

The swagger UI can be found at: https://app.swaggerhub.com/apis/cyledoux/cythereal-magic-api/0.0.0