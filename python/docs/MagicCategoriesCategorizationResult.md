# MagicCategoriesCategorizationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_categories** | **int** |  | [optional] 
**categories** | [**list[MagicCategoriesCategorizationResultCategories]**](MagicCategoriesCategorizationResultCategories.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


