# MagicMatchesSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**similar_packer_similar_payload** | **list[str]** |  | [optional] 
**similar_packer_different_payload** | **list[str]** |  | [optional] 
**different_packer_similar_payload** | **list[str]** |  | [optional] 
**weak_similar** | **list[str]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


