# cythereal_magic.MagicReportApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**magic_categories**](MagicReportApi.md#magic_categories) | **GET** /categories/{api_key}/{file_hash} | Retrieve the MAGIC categories for a binary. **Alpha Level Feature**
[**magic_correlations**](MagicReportApi.md#magic_correlations) | **GET** /magic/{api_key}/{file_hash} | Retrieve the MAGIC correlations for a binary.
[**put_magic_categories**](MagicReportApi.md#put_magic_categories) | **PUT** /categories/{api_key}/{file_hash} | Save category for a binary. **Alpha Level Feature**


# **magic_categories**
> MagicCategoriesResponse magic_categories(api_key, file_hash)

Retrieve the MAGIC categories for a binary. **Alpha Level Feature**

### Example 
```python
from __future__ import print_statement
import time
import cythereal_magic
from cythereal_magic.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cythereal_magic.MagicReportApi()
api_key = 'api_key_example' # str | API Key for accessing the service
file_hash = 'file_hash_example' # str | A cryptographic hash of a file. Supported hashes are SHA1.

try: 
    # Retrieve the MAGIC categories for a binary. **Alpha Level Feature**
    api_response = api_instance.magic_categories(api_key, file_hash)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MagicReportApi->magic_categories: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**| API Key for accessing the service | 
 **file_hash** | **str**| A cryptographic hash of a file. Supported hashes are SHA1. | 

### Return type

[**MagicCategoriesResponse**](MagicCategoriesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **magic_correlations**
> MagicMatchesResponse magic_correlations(api_key, file_hash, details=details, threshold=threshold)

Retrieve the MAGIC correlations for a binary.

### Example 
```python
from __future__ import print_statement
import time
import cythereal_magic
from cythereal_magic.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cythereal_magic.MagicReportApi()
api_key = 'api_key_example' # str | API Key for accessing the service
file_hash = 'file_hash_example' # str | A cryptographic hash of a file. Supported hashes are SHA1.
details = true # bool | If false, omits the match details. Defaults to true. (optional) (default to true)
threshold = 0.7 # float | Only similarity matches at value equal or above threshold will be considered. MUST be in range [0, 1]. Defaults to 0.7. (optional) (default to 0.7)

try: 
    # Retrieve the MAGIC correlations for a binary.
    api_response = api_instance.magic_correlations(api_key, file_hash, details=details, threshold=threshold)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MagicReportApi->magic_correlations: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**| API Key for accessing the service | 
 **file_hash** | **str**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **details** | **bool**| If false, omits the match details. Defaults to true. | [optional] [default to true]
 **threshold** | **float**| Only similarity matches at value equal or above threshold will be considered. MUST be in range [0, 1]. Defaults to 0.7. | [optional] [default to 0.7]

### Return type

[**MagicMatchesResponse**](MagicMatchesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **put_magic_categories**
> ResponseObject put_magic_categories(api_key, file_hash, category)

Save category for a binary. **Alpha Level Feature**

### Example 
```python
from __future__ import print_statement
import time
import cythereal_magic
from cythereal_magic.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cythereal_magic.MagicReportApi()
api_key = 'api_key_example' # str | API Key for accessing the service
file_hash = 'file_hash_example' # str | A cryptographic hash of a file. Supported hashes are SHA1.
category = 'category_example' # str | Category assigned to the file.

try: 
    # Save category for a binary. **Alpha Level Feature**
    api_response = api_instance.put_magic_categories(api_key, file_hash, category)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling MagicReportApi->put_magic_categories: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**| API Key for accessing the service | 
 **file_hash** | **str**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **category** | **str**| Category assigned to the file. | 

### Return type

[**ResponseObject**](ResponseObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

