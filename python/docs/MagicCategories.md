# MagicCategories

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ground_truth** | [**MagicCategoriesGroundTruth**](MagicCategoriesGroundTruth.md) |  | [optional] 
**categorization_result** | [**MagicCategoriesCategorizationResult**](MagicCategoriesCategorizationResult.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


