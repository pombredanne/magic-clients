# cythereal_magic.UserOperationsApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**register**](UserOperationsApi.md#register) | **GET** /register | Register to receive a MAGIC API key.


# **register**
> ResponseObject register(email, name)

Register to receive a MAGIC API key.

### Example 
```python
from __future__ import print_statement
import time
import cythereal_magic
from cythereal_magic.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cythereal_magic.UserOperationsApi()
email = 'email_example' # str | Email Address
name = 'name_example' # str | Full Name

try: 
    # Register to receive a MAGIC API key.
    api_response = api_instance.register(email, name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserOperationsApi->register: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **str**| Email Address | 
 **name** | **str**| Full Name | 

### Return type

[**ResponseObject**](ResponseObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

