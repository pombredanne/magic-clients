# cythereal_magic.SimilarityApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**similar_binaries**](SimilarityApi.md#similar_binaries) | **GET** /search/binary/{api_key}/{file_hash} | Search for similar binaries.
[**similar_procedures**](SimilarityApi.md#similar_procedures) | **GET** /search/procs/{api_key}/{file_hash}/{proc_rva} | Search for procedures similar to a given procedure.


# **similar_binaries**
> BinarySimilarityResponse similar_binaries(api_key, file_hash, threshold=threshold, level=level)

Search for similar binaries.

### Example 
```python
from __future__ import print_statement
import time
import cythereal_magic
from cythereal_magic.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cythereal_magic.SimilarityApi()
api_key = 'api_key_example' # str | API Key for accessing the service
file_hash = 'file_hash_example' # str | A cryptographic hash of a file. Supported hashes are SHA1.
threshold = 0.7 # float | Similarity threshold, default 0.7. (optional) (default to 0.7)
level = 3 # int | Similarity Search Level from 1 through 5, default is 3. (optional) (default to 3)

try: 
    # Search for similar binaries.
    api_response = api_instance.similar_binaries(api_key, file_hash, threshold=threshold, level=level)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SimilarityApi->similar_binaries: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**| API Key for accessing the service | 
 **file_hash** | **str**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **threshold** | **float**| Similarity threshold, default 0.7. | [optional] [default to 0.7]
 **level** | **int**| Similarity Search Level from 1 through 5, default is 3. | [optional] [default to 3]

### Return type

[**BinarySimilarityResponse**](BinarySimilarityResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **similar_procedures**
> ProcedureSimilarityResponse similar_procedures(api_key, file_hash, proc_rva)

Search for procedures similar to a given procedure.

### Example 
```python
from __future__ import print_statement
import time
import cythereal_magic
from cythereal_magic.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cythereal_magic.SimilarityApi()
api_key = 'api_key_example' # str | API Key for accessing the service
file_hash = 'file_hash_example' # str | A cryptographic hash of a file. Supported hashes are SHA1.
proc_rva = 'proc_rva_example' # str | The RVA of the procedure to find similarities with. Formatted 0xnnnn

try: 
    # Search for procedures similar to a given procedure.
    api_response = api_instance.similar_procedures(api_key, file_hash, proc_rva)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SimilarityApi->similar_procedures: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**| API Key for accessing the service | 
 **file_hash** | **str**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **proc_rva** | **str**| The RVA of the procedure to find similarities with. Formatted 0xnnnn | 

### Return type

[**ProcedureSimilarityResponse**](ProcedureSimilarityResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

