# ProcedureSimilaritySemanticallyEquivalentProcedures

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**proc_name** | **str** | Name of the matched procedure. | [optional] 
**proc_id** | **str** | ID of the matched procedure. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


