# MagicMatches

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**version** | **str** | Version of the MAGIC report. This is different from the API version. | 
**query** | [**MagicMatchesQuery**](MagicMatchesQuery.md) |  | [optional] 
**summary** | [**MagicMatchesSummary**](MagicMatchesSummary.md) |  | [optional] 
**details** | [**list[MagicMatchesDetails]**](MagicMatchesDetails.md) | Contains details about the similarity between the query binary and the MAGIC matches. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


