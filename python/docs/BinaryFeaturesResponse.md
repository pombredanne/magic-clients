# BinaryFeaturesResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **str** | Indicates whether an operation was successful or not. | 
**statuscode** | **int** | Status code for the operation. This is NOT the same as an HTTP status code. It is closer to a unix exit code. Zero indicates success. Non-zero indicates failure. | 
**vb_version** | **str** |  | 
**message** | **str** | A message for the user. Current version of the API. | [optional] 
**hash** | **str** | The hash value used in the query. | [optional] 
**answer** | [**dict(str, ProcedureFeatures)**](ProcedureFeatures.md) | A mapping object that maps the RVA of all procedures in the binary to the genomic features of that procedure. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


