# FileInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sha1** | **str** |  | 
**md5** | **str** |  | 
**unix_filetype** | **str** | Output of the unix &#x60;file&#x60; command for the file. | [optional] 
**filepath** | **list[str]** | The original file paths associated with the file. | [optional] 
**length** | **int** | Size of the file. | [optional] 
**parents** | **list[str]** | The SHA1 of the file(s) that this file was generated from. | [optional] 
**upload_date** | **datetime** | The timestamp of the original upload | [optional] 
**object_class** | **str** | An internal indicator of the type of object this file is. | 
**children** | [**list[ChildFileInfo]**](ChildFileInfo.md) | The list of files that were created from this file. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


