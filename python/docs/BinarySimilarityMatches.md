# BinarySimilarityMatches

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file_hash** | **str** | SHA1 of a similar binary | [optional] 
**similarity** | **float** | Similarity score of the matched binary | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


