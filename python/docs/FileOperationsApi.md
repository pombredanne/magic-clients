# cythereal_magic.FileOperationsApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**download**](FileOperationsApi.md#download) | **GET** /download/{api_key}/{file_hash} | Download a file
[**query**](FileOperationsApi.md#query) | **GET** /query/{api_key}/{file_hash} | Query file info and analysis status
[**reprocess**](FileOperationsApi.md#reprocess) | **GET** /reprocess/{api_key}/{file_hash} | Reprocess a previously uploaded file
[**upload**](FileOperationsApi.md#upload) | **POST** /upload/{api_key} | Upload file for processing


# **download**
> file download(api_key, file_hash)

Download a file

### Example 
```python
from __future__ import print_statement
import time
import cythereal_magic
from cythereal_magic.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cythereal_magic.FileOperationsApi()
api_key = 'api_key_example' # str | API Key for accessing the service
file_hash = 'file_hash_example' # str | A cryptographic hash of a file. Supported hashes are SHA1.

try: 
    # Download a file
    api_response = api_instance.download(api_key, file_hash)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FileOperationsApi->download: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**| API Key for accessing the service | 
 **file_hash** | **str**| A cryptographic hash of a file. Supported hashes are SHA1. | 

### Return type

[**file**](file.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **query**
> QueryResponse query(api_key, file_hash)

Query file info and analysis status

### Example 
```python
from __future__ import print_statement
import time
import cythereal_magic
from cythereal_magic.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cythereal_magic.FileOperationsApi()
api_key = 'api_key_example' # str | API Key for accessing the service
file_hash = 'file_hash_example' # str | A cryptographic hash of a file. Supported hashes are SHA1.

try: 
    # Query file info and analysis status
    api_response = api_instance.query(api_key, file_hash)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FileOperationsApi->query: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**| API Key for accessing the service | 
 **file_hash** | **str**| A cryptographic hash of a file. Supported hashes are SHA1. | 

### Return type

[**QueryResponse**](QueryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **reprocess**
> ResponseObject reprocess(api_key, file_hash, priority=priority)

Reprocess a previously uploaded file

### Example 
```python
from __future__ import print_statement
import time
import cythereal_magic
from cythereal_magic.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cythereal_magic.FileOperationsApi()
api_key = 'api_key_example' # str | API Key for accessing the service
file_hash = 'file_hash_example' # str | A cryptographic hash of a file. Supported hashes are SHA1.
priority = 2 # int | Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. (optional) (default to 2)

try: 
    # Reprocess a previously uploaded file
    api_response = api_instance.reprocess(api_key, file_hash, priority=priority)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FileOperationsApi->reprocess: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**| API Key for accessing the service | 
 **file_hash** | **str**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **priority** | **int**| Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. | [optional] [default to 2]

### Return type

[**ResponseObject**](ResponseObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upload**
> ResponseObject upload(api_key, filedata, orig_file_path=orig_file_path, password=password, priority=priority, force=force)

Upload file for processing

### Example 
```python
from __future__ import print_statement
import time
import cythereal_magic
from cythereal_magic.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cythereal_magic.FileOperationsApi()
api_key = 'api_key_example' # str | API Key for accessing the service
filedata = '/path/to/file.txt' # file | Binary contents of the file
orig_file_path = 'orig_file_path_example' # str | Name of the file being uploaded (optional)
password = 'password_example' # str | If uploading a password protected zip, this field MUST contain the password. (optional)
priority = 2 # int | Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. (optional) (default to 2)
force = false # bool | If true, will force existing file to be reanalyzed. (optional) (default to false)

try: 
    # Upload file for processing
    api_response = api_instance.upload(api_key, filedata, orig_file_path=orig_file_path, password=password, priority=priority, force=force)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FileOperationsApi->upload: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**| API Key for accessing the service | 
 **filedata** | **file**| Binary contents of the file | 
 **orig_file_path** | **str**| Name of the file being uploaded | [optional] 
 **password** | **str**| If uploading a password protected zip, this field MUST contain the password. | [optional] 
 **priority** | **int**| Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. | [optional] [default to 2]
 **force** | **bool**| If true, will force existing file to be reanalyzed. | [optional] [default to false]

### Return type

[**ResponseObject**](ResponseObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

