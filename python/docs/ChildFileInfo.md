# ChildFileInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**service_name** | **str** | Name of the analysis that created the child file. | 
**status** | **str** | Status of the analysis that created the child file. | 
**child** | **str** | SHA1 of the child file. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


