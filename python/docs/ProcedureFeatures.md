# ProcedureFeatures

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The ID of the function. It takes the form \&quot;&#x60;binary_id&#x60;/&#x60;startRVA&#x60;\&quot;. | [optional] 
**binary_id** | **str** | The SHA1 of the binary this function is in. | [optional] 
**hard_hash** | **str** | This is a special hash created by MAGIC to identify semantically equivalent functions. If two functions have an equivalent hardHash, then the semantics of both functions are the same, i.e. the functions can be considered functionally equivalent. | [optional] 
**start_rva** | **str** | The relative virtual address of the function | [optional] 
**is_library** | **bool** | Indicates  if this function was identified as a library function. | [optional] 
**is_thunk** | **bool** | Indicates if this function was identified as a thunk (jump) function. | [optional] 
**pe_segment** | **str** | Lists the [PE Segment](https://msdn.microsoft.com/en-us/library/ms809762.aspx) that the function was located in. | [optional] 
**proc_name** | **str** | The name of the procedure. If debug symbols were not present in the original file, this will be of the form &#x60;sub_xxxxxx&#x60; where &#x60;xxxxxx&#x60; is the virtual address of the function. | [optional] 
**api_calls** | **list[str]** | The list of APIs called from this function. | [optional] 
**code** | **list[list[str]]** | The disassembly of this function. This is a list of lists where each sublist represents a disassembly block. Each block contains a list of the instructions that compose the block. | [optional] 
**code_size** | **int** | The number of instructions in the disassembly. | [optional] 
**gen_code** | **list[list[str]]** | Generalized disassembly. This is the same as &#x60;code&#x60;, but with the operands in code abstracted to variables instead of register names. | [optional] 
**gen_code_size** | **int** | The number of generalized instructions in &#x60;gen_code&#x60;. | [optional] 
**semantics** | **list[list[str]]** | The semantics of the instructions in &#x60;code&#x60;. This is a list of lists where each inner list corresponds to a block from &#x60;code&#x60;. This inner list contains the effect that executing the represented block will have on registers and memory locations. | [optional] 
**semantics_size** | **int** | The number of individual semantics statements in &#x60;semantics&#x60; | [optional] 
**gen_semantics** | **list[list[str]]** | Generalized semantics. This is the same as &#x60;semantics&#x60; but with registers and memory locations abstracted to variables. | [optional] 
**gen_semantics_size** | **int** | The number of individual semantics statements in &#x60;gen_semantics&#x60; | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


