# cythereal_magic.GenomicFeaturesApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**binary_features**](GenomicFeaturesApi.md#binary_features) | **GET** /show/binary/{api_key}/{file_hash} | Show the genomic features for a given binary.
[**procedure_features**](GenomicFeaturesApi.md#procedure_features) | **GET** /show/proc/{api_key}/{file_hash}/{proc_rva} | Show the genomic features for a given procedure.


# **binary_features**
> BinaryFeaturesResponse binary_features(api_key, file_hash)

Show the genomic features for a given binary.

### Example 
```python
from __future__ import print_statement
import time
import cythereal_magic
from cythereal_magic.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cythereal_magic.GenomicFeaturesApi()
api_key = 'api_key_example' # str | API Key for accessing the service
file_hash = 'file_hash_example' # str | A cryptographic hash of a file. Supported hashes are SHA1.

try: 
    # Show the genomic features for a given binary.
    api_response = api_instance.binary_features(api_key, file_hash)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GenomicFeaturesApi->binary_features: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**| API Key for accessing the service | 
 **file_hash** | **str**| A cryptographic hash of a file. Supported hashes are SHA1. | 

### Return type

[**BinaryFeaturesResponse**](BinaryFeaturesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **procedure_features**
> ProcedureFeaturesResponse procedure_features(api_key, file_hash, proc_rva)

Show the genomic features for a given procedure.

### Example 
```python
from __future__ import print_statement
import time
import cythereal_magic
from cythereal_magic.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = cythereal_magic.GenomicFeaturesApi()
api_key = 'api_key_example' # str | API Key for accessing the service
file_hash = 'file_hash_example' # str | A cryptographic hash of a file. Supported hashes are SHA1.
proc_rva = 'proc_rva_example' # str | The RVA of the procedure to find similarities with. Formatted 0xnnnn

try: 
    # Show the genomic features for a given procedure.
    api_response = api_instance.procedure_features(api_key, file_hash, proc_rva)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling GenomicFeaturesApi->procedure_features: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **api_key** | **str**| API Key for accessing the service | 
 **file_hash** | **str**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **proc_rva** | **str**| The RVA of the procedure to find similarities with. Formatted 0xnnnn | 

### Return type

[**ProcedureFeaturesResponse**](ProcedureFeaturesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

