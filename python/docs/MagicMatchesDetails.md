# MagicMatchesDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sha1** | **str** | The SHA1 of the matched binary. | [optional] 
**match_type** | **str** | The MAGIC classification for the match. | [optional] 
**match_subtypes** | [**list[MagicMatchesDetailsMatchSubtypes]**](MagicMatchesDetailsMatchSubtypes.md) | Finer grained identification of match types. | [optional] 
**max_similarity** | **float** | The maximum similarity value from &#x60;match_subtypes&#x60;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


