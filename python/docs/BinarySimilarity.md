# BinarySimilarity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**matches** | [**list[BinarySimilarityMatches]**](BinarySimilarityMatches.md) | List of binaries similar to the query binary. Sorted by decreasing similarity. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


