# coding: utf-8

"""
    Cythereal MAGIC API

     ##### Example Inputs Here are some example inputs that can be used for testing the service. * Binary SHA1: `ff9790d7902fea4c910b182f6e0b00221a40d616`   * Can be used for `file_hash` parameters. * Procedure RVA: `0x1000`   * Use with the above SHA1 for `proc_rva` parameters.

    OpenAPI spec version: 0.0.0
    Contact: support@cythereal.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

from __future__ import absolute_import

import sys
import os
import re

# python 2 and python 3 compatibility library
from six import iteritems

from ..configuration import Configuration
from ..api_client import ApiClient


class MagicReportApi(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        config = Configuration()
        if api_client:
            self.api_client = api_client
        else:
            if not config.api_client:
                config.api_client = ApiClient()
            self.api_client = config.api_client

    def magic_categories(self, api_key, file_hash, **kwargs):
        """
        Retrieve the MAGIC categories for a binary. **Alpha Level Feature**
        

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.magic_categories(api_key, file_hash, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str api_key: API Key for accessing the service (required)
        :param str file_hash: A cryptographic hash of a file. Supported hashes are SHA1. (required)
        :return: MagicCategoriesResponse
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.magic_categories_with_http_info(api_key, file_hash, **kwargs)
        else:
            (data) = self.magic_categories_with_http_info(api_key, file_hash, **kwargs)
            return data

    def magic_categories_with_http_info(self, api_key, file_hash, **kwargs):
        """
        Retrieve the MAGIC categories for a binary. **Alpha Level Feature**
        

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.magic_categories_with_http_info(api_key, file_hash, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str api_key: API Key for accessing the service (required)
        :param str file_hash: A cryptographic hash of a file. Supported hashes are SHA1. (required)
        :return: MagicCategoriesResponse
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['api_key', 'file_hash']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method magic_categories" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'api_key' is set
        if ('api_key' not in params) or (params['api_key'] is None):
            raise ValueError("Missing the required parameter `api_key` when calling `magic_categories`")
        # verify the required parameter 'file_hash' is set
        if ('file_hash' not in params) or (params['file_hash'] is None):
            raise ValueError("Missing the required parameter `file_hash` when calling `magic_categories`")

        if 'file_hash' in params and not re.search('[a-fA-F0-9]{40}', params['file_hash']):
            raise ValueError("Invalid value for parameter `file_hash` when calling `magic_categories`, must conform to the pattern `/[a-fA-F0-9]{40}/`")

        collection_formats = {}

        resource_path = '/categories/{api_key}/{file_hash}'.replace('{format}', 'json')
        path_params = {}
        if 'api_key' in params:
            path_params['api_key'] = params['api_key']
        if 'file_hash' in params:
            path_params['file_hash'] = params['file_hash']

        query_params = {}

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None

        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])
        if not header_params['Accept']:
            del header_params['Accept']

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type([])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api(resource_path, 'GET',
                                            path_params,
                                            query_params,
                                            header_params,
                                            body=body_params,
                                            post_params=form_params,
                                            files=local_var_files,
                                            response_type='MagicCategoriesResponse',
                                            auth_settings=auth_settings,
                                            callback=params.get('callback'),
                                            _return_http_data_only=params.get('_return_http_data_only'),
                                            _preload_content=params.get('_preload_content', True),
                                            _request_timeout=params.get('_request_timeout'),
                                            collection_formats=collection_formats)

    def magic_correlations(self, api_key, file_hash, **kwargs):
        """
        Retrieve the MAGIC correlations for a binary.
        

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.magic_correlations(api_key, file_hash, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str api_key: API Key for accessing the service (required)
        :param str file_hash: A cryptographic hash of a file. Supported hashes are SHA1. (required)
        :param bool details: If false, omits the match details. Defaults to true.
        :param float threshold: Only similarity matches at value equal or above threshold will be considered. MUST be in range [0, 1]. Defaults to 0.7.
        :return: MagicMatchesResponse
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.magic_correlations_with_http_info(api_key, file_hash, **kwargs)
        else:
            (data) = self.magic_correlations_with_http_info(api_key, file_hash, **kwargs)
            return data

    def magic_correlations_with_http_info(self, api_key, file_hash, **kwargs):
        """
        Retrieve the MAGIC correlations for a binary.
        

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.magic_correlations_with_http_info(api_key, file_hash, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str api_key: API Key for accessing the service (required)
        :param str file_hash: A cryptographic hash of a file. Supported hashes are SHA1. (required)
        :param bool details: If false, omits the match details. Defaults to true.
        :param float threshold: Only similarity matches at value equal or above threshold will be considered. MUST be in range [0, 1]. Defaults to 0.7.
        :return: MagicMatchesResponse
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['api_key', 'file_hash', 'details', 'threshold']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method magic_correlations" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'api_key' is set
        if ('api_key' not in params) or (params['api_key'] is None):
            raise ValueError("Missing the required parameter `api_key` when calling `magic_correlations`")
        # verify the required parameter 'file_hash' is set
        if ('file_hash' not in params) or (params['file_hash'] is None):
            raise ValueError("Missing the required parameter `file_hash` when calling `magic_correlations`")

        if 'file_hash' in params and not re.search('[a-fA-F0-9]{40}', params['file_hash']):
            raise ValueError("Invalid value for parameter `file_hash` when calling `magic_correlations`, must conform to the pattern `/[a-fA-F0-9]{40}/`")

        collection_formats = {}

        resource_path = '/magic/{api_key}/{file_hash}'.replace('{format}', 'json')
        path_params = {}
        if 'api_key' in params:
            path_params['api_key'] = params['api_key']
        if 'file_hash' in params:
            path_params['file_hash'] = params['file_hash']

        query_params = {}
        if 'details' in params:
            query_params['details'] = params['details']
        if 'threshold' in params:
            query_params['threshold'] = params['threshold']

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None

        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])
        if not header_params['Accept']:
            del header_params['Accept']

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type([])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api(resource_path, 'GET',
                                            path_params,
                                            query_params,
                                            header_params,
                                            body=body_params,
                                            post_params=form_params,
                                            files=local_var_files,
                                            response_type='MagicMatchesResponse',
                                            auth_settings=auth_settings,
                                            callback=params.get('callback'),
                                            _return_http_data_only=params.get('_return_http_data_only'),
                                            _preload_content=params.get('_preload_content', True),
                                            _request_timeout=params.get('_request_timeout'),
                                            collection_formats=collection_formats)

    def put_magic_categories(self, api_key, file_hash, category, **kwargs):
        """
        Save category for a binary. **Alpha Level Feature**
        

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.put_magic_categories(api_key, file_hash, category, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str api_key: API Key for accessing the service (required)
        :param str file_hash: A cryptographic hash of a file. Supported hashes are SHA1. (required)
        :param str category: Category assigned to the file. (required)
        :return: ResponseObject
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('callback'):
            return self.put_magic_categories_with_http_info(api_key, file_hash, category, **kwargs)
        else:
            (data) = self.put_magic_categories_with_http_info(api_key, file_hash, category, **kwargs)
            return data

    def put_magic_categories_with_http_info(self, api_key, file_hash, category, **kwargs):
        """
        Save category for a binary. **Alpha Level Feature**
        

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please define a `callback` function
        to be invoked when receiving the response.
        >>> def callback_function(response):
        >>>     pprint(response)
        >>>
        >>> thread = api.put_magic_categories_with_http_info(api_key, file_hash, category, callback=callback_function)

        :param callback function: The callback function
            for asynchronous request. (optional)
        :param str api_key: API Key for accessing the service (required)
        :param str file_hash: A cryptographic hash of a file. Supported hashes are SHA1. (required)
        :param str category: Category assigned to the file. (required)
        :return: ResponseObject
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['api_key', 'file_hash', 'category']
        all_params.append('callback')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method put_magic_categories" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'api_key' is set
        if ('api_key' not in params) or (params['api_key'] is None):
            raise ValueError("Missing the required parameter `api_key` when calling `put_magic_categories`")
        # verify the required parameter 'file_hash' is set
        if ('file_hash' not in params) or (params['file_hash'] is None):
            raise ValueError("Missing the required parameter `file_hash` when calling `put_magic_categories`")
        # verify the required parameter 'category' is set
        if ('category' not in params) or (params['category'] is None):
            raise ValueError("Missing the required parameter `category` when calling `put_magic_categories`")

        if 'file_hash' in params and not re.search('[a-fA-F0-9]{40}', params['file_hash']):
            raise ValueError("Invalid value for parameter `file_hash` when calling `put_magic_categories`, must conform to the pattern `/[a-fA-F0-9]{40}/`")

        collection_formats = {}

        resource_path = '/categories/{api_key}/{file_hash}'.replace('{format}', 'json')
        path_params = {}
        if 'api_key' in params:
            path_params['api_key'] = params['api_key']
        if 'file_hash' in params:
            path_params['file_hash'] = params['file_hash']

        query_params = {}

        header_params = {}

        form_params = []
        local_var_files = {}
        if 'category' in params:
            form_params.append(('category', params['category']))

        body_params = None

        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.\
            select_header_accept(['application/json'])
        if not header_params['Accept']:
            del header_params['Accept']

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.\
            select_header_content_type(['application/x-www-form-urlencoded'])

        # Authentication setting
        auth_settings = []

        return self.api_client.call_api(resource_path, 'PUT',
                                            path_params,
                                            query_params,
                                            header_params,
                                            body=body_params,
                                            post_params=form_params,
                                            files=local_var_files,
                                            response_type='ResponseObject',
                                            auth_settings=auth_settings,
                                            callback=params.get('callback'),
                                            _return_http_data_only=params.get('_return_http_data_only'),
                                            _preload_content=params.get('_preload_content', True),
                                            _request_timeout=params.get('_request_timeout'),
                                            collection_formats=collection_formats)
