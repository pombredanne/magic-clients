/* 
 * Cythereal MAGIC API
 *
 *  ##### Example Inputs Here are some example inputs that can be used for testing the service. * Binary SHA1: `ff9790d7902fea4c910b182f6e0b00221a40d616`   * Can be used for `file_hash` parameters. * Procedure RVA: `0x1000`   * Use with the above SHA1 for `proc_rva` parameters.
 *
 * OpenAPI spec version: 0.0.0
 * Contact: support@cythereal.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cythereal

import (
	"net/url"
	"strings"
	"encoding/json"
	"fmt"
)

type SimilarityApi struct {
	Configuration *Configuration
}

func NewSimilarityApi() *SimilarityApi {
	configuration := NewConfiguration()
	return &SimilarityApi{
		Configuration: configuration,
	}
}

func NewSimilarityApiWithBasePath(basePath string) *SimilarityApi {
	configuration := NewConfiguration()
	configuration.BasePath = basePath

	return &SimilarityApi{
		Configuration: configuration,
	}
}

/**
 * Search for similar binaries.
 *
 * @param apiKey API Key for accessing the service
 * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1.
 * @param threshold Similarity threshold, default 0.7.
 * @param level Similarity Search Level from 1 through 5, default is 3.
 * @return *BinarySimilarityResponse
 */
func (a SimilarityApi) SimilarBinaries(apiKey string, fileHash string, threshold float32, level int32) (*BinarySimilarityResponse, *APIResponse, error) {

	var localVarHttpMethod = strings.ToUpper("Get")
	// create path and map variables
	localVarPath := a.Configuration.BasePath + "/search/binary/{api_key}/{file_hash}"
	localVarPath = strings.Replace(localVarPath, "{"+"api_key"+"}", fmt.Sprintf("%v", apiKey), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"file_hash"+"}", fmt.Sprintf("%v", fileHash), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := make(map[string]string)
	var localVarPostBody interface{}
	var localVarFileName string
	var localVarFileBytes []byte
	// add default headers if any
	for key := range a.Configuration.DefaultHeader {
		localVarHeaderParams[key] = a.Configuration.DefaultHeader[key]
	}
		localVarQueryParams.Add("threshold", a.Configuration.APIClient.ParameterToString(threshold, ""))
		localVarQueryParams.Add("level", a.Configuration.APIClient.ParameterToString(level, ""))

	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := a.Configuration.APIClient.SelectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}
	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := a.Configuration.APIClient.SelectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	var successPayload = new(BinarySimilarityResponse)
	localVarHttpResponse, err := a.Configuration.APIClient.CallAPI(localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)

	var localVarURL, _ = url.Parse(localVarPath)
	localVarURL.RawQuery = localVarQueryParams.Encode()
	var localVarAPIResponse = &APIResponse{Operation: "SimilarBinaries", Method: localVarHttpMethod, RequestURL: localVarURL.String()}
	if localVarHttpResponse != nil {
		localVarAPIResponse.Response = localVarHttpResponse.RawResponse
		localVarAPIResponse.Payload = localVarHttpResponse.Body()
	}

	if err != nil {
		return successPayload, localVarAPIResponse, err
	}
	err = json.Unmarshal(localVarHttpResponse.Body(), &successPayload)
	return successPayload, localVarAPIResponse, err
}

/**
 * Search for procedures similar to a given procedure.
 *
 * @param apiKey API Key for accessing the service
 * @param fileHash A cryptographic hash of a file. Supported hashes are SHA1.
 * @param procRva The RVA of the procedure to find similarities with. Formatted 0xnnnn
 * @return *ProcedureSimilarityResponse
 */
func (a SimilarityApi) SimilarProcedures(apiKey string, fileHash string, procRva string) (*ProcedureSimilarityResponse, *APIResponse, error) {

	var localVarHttpMethod = strings.ToUpper("Get")
	// create path and map variables
	localVarPath := a.Configuration.BasePath + "/search/procs/{api_key}/{file_hash}/{proc_rva}"
	localVarPath = strings.Replace(localVarPath, "{"+"api_key"+"}", fmt.Sprintf("%v", apiKey), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"file_hash"+"}", fmt.Sprintf("%v", fileHash), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"proc_rva"+"}", fmt.Sprintf("%v", procRva), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := make(map[string]string)
	var localVarPostBody interface{}
	var localVarFileName string
	var localVarFileBytes []byte
	// add default headers if any
	for key := range a.Configuration.DefaultHeader {
		localVarHeaderParams[key] = a.Configuration.DefaultHeader[key]
	}

	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := a.Configuration.APIClient.SelectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}
	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := a.Configuration.APIClient.SelectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	var successPayload = new(ProcedureSimilarityResponse)
	localVarHttpResponse, err := a.Configuration.APIClient.CallAPI(localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)

	var localVarURL, _ = url.Parse(localVarPath)
	localVarURL.RawQuery = localVarQueryParams.Encode()
	var localVarAPIResponse = &APIResponse{Operation: "SimilarProcedures", Method: localVarHttpMethod, RequestURL: localVarURL.String()}
	if localVarHttpResponse != nil {
		localVarAPIResponse.Response = localVarHttpResponse.RawResponse
		localVarAPIResponse.Payload = localVarHttpResponse.Body()
	}

	if err != nil {
		return successPayload, localVarAPIResponse, err
	}
	err = json.Unmarshal(localVarHttpResponse.Body(), &successPayload)
	return successPayload, localVarAPIResponse, err
}

