# MagicCategories

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**GroundTruth** | [**MagicCategoriesGroundTruth**](MagicCategories_ground_truth.md) |  | [optional] [default to null]
**CategorizationResult** | [**MagicCategoriesCategorizationResult**](MagicCategories_categorization_result.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


