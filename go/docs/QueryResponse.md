# QueryResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Status** | **string** | Indicates whether an operation was successful or not. | [default to null]
**Statuscode** | **int32** | Status code for the operation. This is NOT the same as an HTTP status code. It is closer to a unix exit code. Zero indicates success. Non-zero indicates failure. | [default to null]
**VbVersion** | **string** |  | [default to null]
**Message** | **string** | A message for the user. Current version of the API. | [optional] [default to null]
**Hash** | **string** | The hash value used in the query. | [optional] [default to null]
**Answer** | [**FileInfo**](FileInfo.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


