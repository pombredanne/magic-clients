# MagicMatches

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Version** | **string** | Version of the MAGIC report. This is different from the API version. | [default to null]
**Query** | [**MagicMatchesQuery**](MagicMatches_query.md) |  | [optional] [default to null]
**Summary** | [**MagicMatchesSummary**](MagicMatches_summary.md) |  | [optional] [default to null]
**Details** | [**[]MagicMatchesDetails**](MagicMatchesDetails.md) | Contains details about the similarity between the query binary and the MAGIC matches. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


