# \FileOperationsApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Download**](FileOperationsApi.md#Download) | **Get** /download/{api_key}/{file_hash} | Download a file
[**Query**](FileOperationsApi.md#Query) | **Get** /query/{api_key}/{file_hash} | Query file info and analysis status
[**Reprocess**](FileOperationsApi.md#Reprocess) | **Get** /reprocess/{api_key}/{file_hash} | Reprocess a previously uploaded file
[**Upload**](FileOperationsApi.md#Upload) | **Post** /upload/{api_key} | Upload file for processing


# **Download**
> *os.File Download($apiKey, $fileHash)

Download a file


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 

### Return type

[***os.File**](*os.File.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **Query**
> QueryResponse Query($apiKey, $fileHash)

Query file info and analysis status


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 

### Return type

[**QueryResponse**](QueryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **Reprocess**
> ResponseObject Reprocess($apiKey, $fileHash, $priority)

Reprocess a previously uploaded file


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **priority** | **int32**| Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. | [optional] [default to 2]

### Return type

[**ResponseObject**](ResponseObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **Upload**
> ResponseObject Upload($apiKey, $filedata, $origFilePath, $password, $priority, $force)

Upload file for processing


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **filedata** | ***os.File**| Binary contents of the file | 
 **origFilePath** | **string**| Name of the file being uploaded | [optional] 
 **password** | **string**| If uploading a password protected zip, this field MUST contain the password. | [optional] 
 **priority** | **int32**| Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. | [optional] [default to 2]
 **force** | **bool**| If true, will force existing file to be reanalyzed. | [optional] [default to false]

### Return type

[**ResponseObject**](ResponseObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

