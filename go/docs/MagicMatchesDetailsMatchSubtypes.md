# MagicMatchesDetailsMatchSubtypes

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MatchSubtype** | **string** |  | [optional] [default to null]
**Similarity** | **float32** | The similarity score | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


