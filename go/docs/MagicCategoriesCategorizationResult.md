# MagicCategoriesCategorizationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TotalCategories** | **int32** |  | [optional] [default to null]
**Categories** | [**[]MagicCategoriesCategorizationResultCategories**](MagicCategories_categorization_result_categories.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


