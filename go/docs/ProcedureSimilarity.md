# ProcedureSimilarity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SemanticallyEquivalentProcedures** | [**[]ProcedureSimilaritySemanticallyEquivalentProcedures**](ProcedureSimilarity_semantically_equivalent_procedures.md) | List of procedures with the exact semantics as the given procedure. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


