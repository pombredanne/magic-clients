# MagicMatchesSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SimilarPackerSimilarPayload** | **[]string** |  | [optional] [default to null]
**SimilarPackerDifferentPayload** | **[]string** |  | [optional] [default to null]
**DifferentPackerSimilarPayload** | **[]string** |  | [optional] [default to null]
**WeakSimilar** | **[]string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


