# \SimilarityApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**SimilarBinaries**](SimilarityApi.md#SimilarBinaries) | **Get** /search/binary/{api_key}/{file_hash} | Search for similar binaries.
[**SimilarProcedures**](SimilarityApi.md#SimilarProcedures) | **Get** /search/procs/{api_key}/{file_hash}/{proc_rva} | Search for procedures similar to a given procedure.


# **SimilarBinaries**
> BinarySimilarityResponse SimilarBinaries($apiKey, $fileHash, $threshold, $level)

Search for similar binaries.


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **threshold** | **float32**| Similarity threshold, default 0.7. | [optional] [default to 0.7]
 **level** | **int32**| Similarity Search Level from 1 through 5, default is 3. | [optional] [default to 3]

### Return type

[**BinarySimilarityResponse**](BinarySimilarityResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SimilarProcedures**
> ProcedureSimilarityResponse SimilarProcedures($apiKey, $fileHash, $procRva)

Search for procedures similar to a given procedure.


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **procRva** | **string**| The RVA of the procedure to find similarities with. Formatted 0xnnnn | 

### Return type

[**ProcedureSimilarityResponse**](ProcedureSimilarityResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

