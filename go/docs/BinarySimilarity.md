# BinarySimilarity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Matches** | [**[]BinarySimilarityMatches**](BinarySimilarity_matches.md) | List of binaries similar to the query binary. Sorted by decreasing similarity. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


