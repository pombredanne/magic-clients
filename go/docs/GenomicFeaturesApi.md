# \GenomicFeaturesApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**BinaryFeatures**](GenomicFeaturesApi.md#BinaryFeatures) | **Get** /show/binary/{api_key}/{file_hash} | Show the genomic features for a given binary.
[**ProcedureFeatures**](GenomicFeaturesApi.md#ProcedureFeatures) | **Get** /show/proc/{api_key}/{file_hash}/{proc_rva} | Show the genomic features for a given procedure.


# **BinaryFeatures**
> BinaryFeaturesResponse BinaryFeatures($apiKey, $fileHash)

Show the genomic features for a given binary.


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 

### Return type

[**BinaryFeaturesResponse**](BinaryFeaturesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ProcedureFeatures**
> ProcedureFeaturesResponse ProcedureFeatures($apiKey, $fileHash, $procRva)

Show the genomic features for a given procedure.


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **procRva** | **string**| The RVA of the procedure to find similarities with. Formatted 0xnnnn | 

### Return type

[**ProcedureFeaturesResponse**](ProcedureFeaturesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

