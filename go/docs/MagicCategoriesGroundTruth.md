# MagicCategoriesGroundTruth

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TotalCategories** | **int32** |  | [optional] [default to null]
**Categories** | [**[]MagicCategoriesGroundTruthCategories**](MagicCategories_ground_truth_categories.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


