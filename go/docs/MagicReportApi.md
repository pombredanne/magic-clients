# \MagicReportApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**MagicCategories**](MagicReportApi.md#MagicCategories) | **Get** /categories/{api_key}/{file_hash} | Retrieve the MAGIC categories for a binary. **Alpha Level Feature**
[**MagicCorrelations**](MagicReportApi.md#MagicCorrelations) | **Get** /magic/{api_key}/{file_hash} | Retrieve the MAGIC correlations for a binary.
[**PutMagicCategories**](MagicReportApi.md#PutMagicCategories) | **Put** /categories/{api_key}/{file_hash} | Save category for a binary. **Alpha Level Feature**


# **MagicCategories**
> MagicCategoriesResponse MagicCategories($apiKey, $fileHash)

Retrieve the MAGIC categories for a binary. **Alpha Level Feature**


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 

### Return type

[**MagicCategoriesResponse**](MagicCategoriesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **MagicCorrelations**
> MagicMatchesResponse MagicCorrelations($apiKey, $fileHash, $details, $threshold)

Retrieve the MAGIC correlations for a binary.


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **details** | **bool**| If false, omits the match details. Defaults to true. | [optional] [default to true]
 **threshold** | **float32**| Only similarity matches at value equal or above threshold will be considered. MUST be in range [0, 1]. Defaults to 0.7. | [optional] [default to 0.7]

### Return type

[**MagicMatchesResponse**](MagicMatchesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PutMagicCategories**
> ResponseObject PutMagicCategories($apiKey, $fileHash, $category)

Save category for a binary. **Alpha Level Feature**


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **category** | **string**| Category assigned to the file. | 

### Return type

[**ResponseObject**](ResponseObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

