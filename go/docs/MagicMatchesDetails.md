# MagicMatchesDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Sha1** | **string** | The SHA1 of the matched binary. | [optional] [default to null]
**MatchType** | **string** | The MAGIC classification for the match. | [optional] [default to null]
**MatchSubtypes** | [**[]MagicMatchesDetailsMatchSubtypes**](MagicMatchesDetails_match_subtypes.md) | Finer grained identification of match types. | [optional] [default to null]
**MaxSimilarity** | **float32** | The maximum similarity value from &#x60;match_subtypes&#x60;. | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


