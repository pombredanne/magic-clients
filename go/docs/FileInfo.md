# FileInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Sha1** | **string** |  | [default to null]
**Md5** | **string** |  | [default to null]
**UnixFiletype** | **string** | Output of the unix &#x60;file&#x60; command for the file. | [optional] [default to null]
**Filepath** | **[]string** | The original file paths associated with the file. | [optional] [default to null]
**Length** | **int32** | Size of the file. | [optional] [default to null]
**Parents** | **[]string** | The SHA1 of the file(s) that this file was generated from. | [optional] [default to null]
**UploadDate** | [**time.Time**](time.Time.md) | The timestamp of the original upload | [optional] [default to null]
**ObjectClass** | **string** | An internal indicator of the type of object this file is. | [default to null]
**Children** | [**[]ChildFileInfo**](ChildFileInfo.md) | The list of files that were created from this file. | [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


