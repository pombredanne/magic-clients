# BinarySimilarityMatches

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FileHash** | **string** | SHA1 of a similar binary | [optional] [default to null]
**Similarity** | **float32** | Similarity score of the matched binary | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


