/* 
 * Cythereal MAGIC API
 *
 *  ##### Example Inputs Here are some example inputs that can be used for testing the service. * Binary SHA1: `ff9790d7902fea4c910b182f6e0b00221a40d616`   * Can be used for `file_hash` parameters. * Procedure RVA: `0x1000`   * Use with the above SHA1 for `proc_rva` parameters.
 *
 * OpenAPI spec version: 0.0.0
 * Contact: support@cythereal.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace CytherealMagic.Model
{
    /// <summary>
    /// ProcedureSimilaritySemanticallyEquivalentProcedures
    /// </summary>
    [DataContract]
    public partial class ProcedureSimilaritySemanticallyEquivalentProcedures :  IEquatable<ProcedureSimilaritySemanticallyEquivalentProcedures>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcedureSimilaritySemanticallyEquivalentProcedures" /> class.
        /// </summary>
        /// <param name="ProcName">Name of the matched procedure..</param>
        /// <param name="ProcId">ID of the matched procedure..</param>
        public ProcedureSimilaritySemanticallyEquivalentProcedures(string ProcName = null, string ProcId = null)
        {
            this.ProcName = ProcName;
            this.ProcId = ProcId;
        }
        
        /// <summary>
        /// Name of the matched procedure.
        /// </summary>
        /// <value>Name of the matched procedure.</value>
        [DataMember(Name="procName", EmitDefaultValue=false)]
        public string ProcName { get; set; }
        /// <summary>
        /// ID of the matched procedure.
        /// </summary>
        /// <value>ID of the matched procedure.</value>
        [DataMember(Name="proc_id", EmitDefaultValue=false)]
        public string ProcId { get; set; }
        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ProcedureSimilaritySemanticallyEquivalentProcedures {\n");
            sb.Append("  ProcName: ").Append(ProcName).Append("\n");
            sb.Append("  ProcId: ").Append(ProcId).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            return this.Equals(obj as ProcedureSimilaritySemanticallyEquivalentProcedures);
        }

        /// <summary>
        /// Returns true if ProcedureSimilaritySemanticallyEquivalentProcedures instances are equal
        /// </summary>
        /// <param name="other">Instance of ProcedureSimilaritySemanticallyEquivalentProcedures to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(ProcedureSimilaritySemanticallyEquivalentProcedures other)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            if (other == null)
                return false;

            return 
                (
                    this.ProcName == other.ProcName ||
                    this.ProcName != null &&
                    this.ProcName.Equals(other.ProcName)
                ) && 
                (
                    this.ProcId == other.ProcId ||
                    this.ProcId != null &&
                    this.ProcId.Equals(other.ProcId)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;
                // Suitable nullity checks etc, of course :)
                if (this.ProcName != null)
                    hash = hash * 59 + this.ProcName.GetHashCode();
                if (this.ProcId != null)
                    hash = hash * 59 + this.ProcId.GetHashCode();
                return hash;
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        { 
            yield break;
        }
    }

}
