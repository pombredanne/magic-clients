/* 
 * Cythereal MAGIC API
 *
 *  ##### Example Inputs Here are some example inputs that can be used for testing the service. * Binary SHA1: `ff9790d7902fea4c910b182f6e0b00221a40d616`   * Can be used for `file_hash` parameters. * Procedure RVA: `0x1000`   * Use with the above SHA1 for `proc_rva` parameters.
 *
 * OpenAPI spec version: 0.0.0
 * Contact: support@cythereal.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;

namespace CytherealMagic.Model
{
    /// <summary>
    /// Information about the child of a file. Contains the meta-data about the file.
    /// </summary>
    [DataContract]
    public partial class ChildFileInfo :  IEquatable<ChildFileInfo>, IValidatableObject
    {
        /// <summary>
        /// Status of the analysis that created the child file.
        /// </summary>
        /// <value>Status of the analysis that created the child file.</value>
        [JsonConverter(typeof(StringEnumConverter))]
        public enum StatusEnum
        {
            
            /// <summary>
            /// Enum Success for "success"
            /// </summary>
            [EnumMember(Value = "success")]
            Success,
            
            /// <summary>
            /// Enum Failed for "failed"
            /// </summary>
            [EnumMember(Value = "failed")]
            Failed,
            
            /// <summary>
            /// Enum Partial for "partial"
            /// </summary>
            [EnumMember(Value = "partial")]
            Partial
        }

        /// <summary>
        /// Status of the analysis that created the child file.
        /// </summary>
        /// <value>Status of the analysis that created the child file.</value>
        [DataMember(Name="status", EmitDefaultValue=false)]
        public StatusEnum? Status { get; set; }
        /// <summary>
        /// Initializes a new instance of the <see cref="ChildFileInfo" /> class.
        /// </summary>
        [JsonConstructorAttribute]
        protected ChildFileInfo() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="ChildFileInfo" /> class.
        /// </summary>
        /// <param name="ServiceName">Name of the analysis that created the child file. (required).</param>
        /// <param name="Status">Status of the analysis that created the child file. (required).</param>
        /// <param name="Child">SHA1 of the child file..</param>
        public ChildFileInfo(string ServiceName = null, StatusEnum? Status = null, string Child = null)
        {
            // to ensure "ServiceName" is required (not null)
            if (ServiceName == null)
            {
                throw new InvalidDataException("ServiceName is a required property for ChildFileInfo and cannot be null");
            }
            else
            {
                this.ServiceName = ServiceName;
            }
            // to ensure "Status" is required (not null)
            if (Status == null)
            {
                throw new InvalidDataException("Status is a required property for ChildFileInfo and cannot be null");
            }
            else
            {
                this.Status = Status;
            }
            this.Child = Child;
        }
        
        /// <summary>
        /// Name of the analysis that created the child file.
        /// </summary>
        /// <value>Name of the analysis that created the child file.</value>
        [DataMember(Name="service_name", EmitDefaultValue=false)]
        public string ServiceName { get; set; }
        /// <summary>
        /// SHA1 of the child file.
        /// </summary>
        /// <value>SHA1 of the child file.</value>
        [DataMember(Name="child", EmitDefaultValue=false)]
        public string Child { get; set; }
        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ChildFileInfo {\n");
            sb.Append("  ServiceName: ").Append(ServiceName).Append("\n");
            sb.Append("  Status: ").Append(Status).Append("\n");
            sb.Append("  Child: ").Append(Child).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            return this.Equals(obj as ChildFileInfo);
        }

        /// <summary>
        /// Returns true if ChildFileInfo instances are equal
        /// </summary>
        /// <param name="other">Instance of ChildFileInfo to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(ChildFileInfo other)
        {
            // credit: http://stackoverflow.com/a/10454552/677735
            if (other == null)
                return false;

            return 
                (
                    this.ServiceName == other.ServiceName ||
                    this.ServiceName != null &&
                    this.ServiceName.Equals(other.ServiceName)
                ) && 
                (
                    this.Status == other.Status ||
                    this.Status != null &&
                    this.Status.Equals(other.Status)
                ) && 
                (
                    this.Child == other.Child ||
                    this.Child != null &&
                    this.Child.Equals(other.Child)
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            // credit: http://stackoverflow.com/a/263416/677735
            unchecked // Overflow is fine, just wrap
            {
                int hash = 41;
                // Suitable nullity checks etc, of course :)
                if (this.ServiceName != null)
                    hash = hash * 59 + this.ServiceName.GetHashCode();
                if (this.Status != null)
                    hash = hash * 59 + this.Status.GetHashCode();
                if (this.Child != null)
                    hash = hash * 59 + this.Child.GetHashCode();
                return hash;
            }
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        { 
            // Child (string) pattern
            Regex regexChild = new Regex(@"[a-fA-F0-9]{40}", RegexOptions.CultureInvariant);
            if (false == regexChild.Match(this.Child).Success)
            {
                yield return new ValidationResult("Invalid value for Child, must match a pattern of /[a-fA-F0-9]{40}/.", new [] { "Child" });
            }

            yield break;
        }
    }

}
