/* 
 * Cythereal MAGIC API
 *
 *  ##### Example Inputs Here are some example inputs that can be used for testing the service. * Binary SHA1: `ff9790d7902fea4c910b182f6e0b00221a40d616`   * Can be used for `file_hash` parameters. * Procedure RVA: `0x1000`   * Use with the above SHA1 for `proc_rva` parameters.
 *
 * OpenAPI spec version: 0.0.0
 * Contact: support@cythereal.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using CytherealMagic.Api;
using CytherealMagic.Model;
using CytherealMagic.Client;
using System.Reflection;

namespace CytherealMagic.Test
{
    /// <summary>
    ///  Class for testing MagicCategoriesCategorizationResultCategories
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class MagicCategoriesCategorizationResultCategoriesTests
    {
        // TODO uncomment below to declare an instance variable for MagicCategoriesCategorizationResultCategories
        //private MagicCategoriesCategorizationResultCategories instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of MagicCategoriesCategorizationResultCategories
            //instance = new MagicCategoriesCategorizationResultCategories();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of MagicCategoriesCategorizationResultCategories
        /// </summary>
        [Test]
        public void MagicCategoriesCategorizationResultCategoriesInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" MagicCategoriesCategorizationResultCategories
            //Assert.IsInstanceOfType<MagicCategoriesCategorizationResultCategories> (instance, "variable 'instance' is a MagicCategoriesCategorizationResultCategories");
        }

        /// <summary>
        /// Test the property 'Name'
        /// </summary>
        [Test]
        public void NameTest()
        {
            // TODO unit test for the property 'Name'
        }
        /// <summary>
        /// Test the property 'Score'
        /// </summary>
        [Test]
        public void ScoreTest()
        {
            // TODO unit test for the property 'Score'
        }

    }

}
