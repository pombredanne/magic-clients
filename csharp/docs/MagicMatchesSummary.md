# CytherealMagic.Model.MagicMatchesSummary
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SimilarPackerSimilarPayload** | **List&lt;string&gt;** |  | [optional] 
**SimilarPackerDifferentPayload** | **List&lt;string&gt;** |  | [optional] 
**DifferentPackerSimilarPayload** | **List&lt;string&gt;** |  | [optional] 
**WeakSimilar** | **List&lt;string&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

