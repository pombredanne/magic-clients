# CytherealMagic.Model.MagicMatchesDetails
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Sha1** | **string** | The SHA1 of the matched binary. | [optional] 
**MatchType** | **string** | The MAGIC classification for the match. | [optional] 
**MatchSubtypes** | [**List&lt;MagicMatchesDetailsMatchSubtypes&gt;**](MagicMatchesDetailsMatchSubtypes.md) | Finer grained identification of match types. | [optional] 
**MaxSimilarity** | **float?** | The maximum similarity value from &#x60;match_subtypes&#x60;. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

