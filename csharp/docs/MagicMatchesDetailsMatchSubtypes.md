# CytherealMagic.Model.MagicMatchesDetailsMatchSubtypes
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**MatchSubtype** | **string** |  | [optional] 
**Similarity** | **float?** | The similarity score | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

