# CytherealMagic.Model.ChildFileInfo
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ServiceName** | **string** | Name of the analysis that created the child file. | 
**Status** | **string** | Status of the analysis that created the child file. | 
**Child** | **string** | SHA1 of the child file. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

