# CytherealMagic.Model.ProcedureSimilaritySemanticallyEquivalentProcedures
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ProcName** | **string** | Name of the matched procedure. | [optional] 
**ProcId** | **string** | ID of the matched procedure. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

