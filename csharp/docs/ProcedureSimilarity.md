# CytherealMagic.Model.ProcedureSimilarity
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SemanticallyEquivalentProcedures** | [**List&lt;ProcedureSimilaritySemanticallyEquivalentProcedures&gt;**](ProcedureSimilaritySemanticallyEquivalentProcedures.md) | List of procedures with the exact semantics as the given procedure. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

