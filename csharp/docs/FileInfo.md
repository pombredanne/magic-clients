# CytherealMagic.Model.FileInfo
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Sha1** | **string** |  | 
**Md5** | **string** |  | 
**UnixFiletype** | **string** | Output of the unix &#x60;file&#x60; command for the file. | [optional] 
**Filepath** | **List&lt;string&gt;** | The original file paths associated with the file. | [optional] 
**Length** | **int?** | Size of the file. | [optional] 
**Parents** | **List&lt;string&gt;** | The SHA1 of the file(s) that this file was generated from. | [optional] 
**UploadDate** | **DateTime?** | The timestamp of the original upload | [optional] 
**ObjectClass** | **string** | An internal indicator of the type of object this file is. | 
**Children** | [**List&lt;ChildFileInfo&gt;**](ChildFileInfo.md) | The list of files that were created from this file. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

