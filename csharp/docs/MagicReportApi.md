# CytherealMagic.Api.MagicReportApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**MagicCategories**](MagicReportApi.md#magiccategories) | **GET** /categories/{api_key}/{file_hash} | Retrieve the MAGIC categories for a binary. **Alpha Level Feature**
[**MagicCorrelations**](MagicReportApi.md#magiccorrelations) | **GET** /magic/{api_key}/{file_hash} | Retrieve the MAGIC correlations for a binary.
[**PutMagicCategories**](MagicReportApi.md#putmagiccategories) | **PUT** /categories/{api_key}/{file_hash} | Save category for a binary. **Alpha Level Feature**


<a name="magiccategories"></a>
# **MagicCategories**
> MagicCategoriesResponse MagicCategories (string apiKey, string fileHash)

Retrieve the MAGIC categories for a binary. **Alpha Level Feature**

### Example
```csharp
using System;
using System.Diagnostics;
using CytherealMagic.Api;
using CytherealMagic.Client;
using CytherealMagic.Model;

namespace Example
{
    public class MagicCategoriesExample
    {
        public void main()
        {
            
            var apiInstance = new MagicReportApi();
            var apiKey = apiKey_example;  // string | API Key for accessing the service
            var fileHash = fileHash_example;  // string | A cryptographic hash of a file. Supported hashes are SHA1.

            try
            {
                // Retrieve the MAGIC categories for a binary. **Alpha Level Feature**
                MagicCategoriesResponse result = apiInstance.MagicCategories(apiKey, fileHash);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MagicReportApi.MagicCategories: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 

### Return type

[**MagicCategoriesResponse**](MagicCategoriesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="magiccorrelations"></a>
# **MagicCorrelations**
> MagicMatchesResponse MagicCorrelations (string apiKey, string fileHash, bool? details, float? threshold)

Retrieve the MAGIC correlations for a binary.

### Example
```csharp
using System;
using System.Diagnostics;
using CytherealMagic.Api;
using CytherealMagic.Client;
using CytherealMagic.Model;

namespace Example
{
    public class MagicCorrelationsExample
    {
        public void main()
        {
            
            var apiInstance = new MagicReportApi();
            var apiKey = apiKey_example;  // string | API Key for accessing the service
            var fileHash = fileHash_example;  // string | A cryptographic hash of a file. Supported hashes are SHA1.
            var details = true;  // bool? | If false, omits the match details. Defaults to true. (optional)  (default to true)
            var threshold = 3.4;  // float? | Only similarity matches at value equal or above threshold will be considered. MUST be in range [0, 1]. Defaults to 0.7. (optional)  (default to 0.7)

            try
            {
                // Retrieve the MAGIC correlations for a binary.
                MagicMatchesResponse result = apiInstance.MagicCorrelations(apiKey, fileHash, details, threshold);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MagicReportApi.MagicCorrelations: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **details** | **bool?**| If false, omits the match details. Defaults to true. | [optional] [default to true]
 **threshold** | **float?**| Only similarity matches at value equal or above threshold will be considered. MUST be in range [0, 1]. Defaults to 0.7. | [optional] [default to 0.7]

### Return type

[**MagicMatchesResponse**](MagicMatchesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="putmagiccategories"></a>
# **PutMagicCategories**
> ResponseObject PutMagicCategories (string apiKey, string fileHash, string category)

Save category for a binary. **Alpha Level Feature**

### Example
```csharp
using System;
using System.Diagnostics;
using CytherealMagic.Api;
using CytherealMagic.Client;
using CytherealMagic.Model;

namespace Example
{
    public class PutMagicCategoriesExample
    {
        public void main()
        {
            
            var apiInstance = new MagicReportApi();
            var apiKey = apiKey_example;  // string | API Key for accessing the service
            var fileHash = fileHash_example;  // string | A cryptographic hash of a file. Supported hashes are SHA1.
            var category = category_example;  // string | Category assigned to the file.

            try
            {
                // Save category for a binary. **Alpha Level Feature**
                ResponseObject result = apiInstance.PutMagicCategories(apiKey, fileHash, category);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling MagicReportApi.PutMagicCategories: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **category** | **string**| Category assigned to the file. | 

### Return type

[**ResponseObject**](ResponseObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

