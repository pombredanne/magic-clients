# CytherealMagic.Model.MagicCategories
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**GroundTruth** | [**MagicCategoriesGroundTruth**](MagicCategoriesGroundTruth.md) |  | [optional] 
**CategorizationResult** | [**MagicCategoriesCategorizationResult**](MagicCategoriesCategorizationResult.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

