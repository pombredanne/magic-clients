# CytherealMagic.Model.BinarySimilarityMatches
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**FileHash** | **string** | SHA1 of a similar binary | [optional] 
**Similarity** | **float?** | Similarity score of the matched binary | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

