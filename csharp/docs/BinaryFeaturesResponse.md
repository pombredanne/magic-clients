# CytherealMagic.Model.BinaryFeaturesResponse
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Statuscode** | **int?** | Status code for the operation. This is NOT the same as an HTTP status code. It is closer to a unix exit code. Zero indicates success. Non-zero indicates failure. | 
**VbVersion** | **string** |  | 
**Message** | **string** | A message for the user. Current version of the API. | [optional] 
**Hash** | **string** | The hash value used in the query. | [optional] 
**Answer** | [**Dictionary&lt;string, ProcedureFeatures&gt;**](ProcedureFeatures.md) | A mapping object that maps the RVA of all procedures in the binary to the genomic features of that procedure. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

