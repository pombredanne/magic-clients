# CytherealMagic.Model.MagicMatches
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Version** | **string** | Version of the MAGIC report. This is different from the API version. | 
**Query** | [**MagicMatchesQuery**](MagicMatchesQuery.md) |  | [optional] 
**Summary** | [**MagicMatchesSummary**](MagicMatchesSummary.md) |  | [optional] 
**Details** | [**List&lt;MagicMatchesDetails&gt;**](MagicMatchesDetails.md) | Contains details about the similarity between the query binary and the MAGIC matches. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

