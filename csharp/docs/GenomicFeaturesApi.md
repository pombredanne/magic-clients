# CytherealMagic.Api.GenomicFeaturesApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**BinaryFeatures**](GenomicFeaturesApi.md#binaryfeatures) | **GET** /show/binary/{api_key}/{file_hash} | Show the genomic features for a given binary.
[**ProcedureFeatures**](GenomicFeaturesApi.md#procedurefeatures) | **GET** /show/proc/{api_key}/{file_hash}/{proc_rva} | Show the genomic features for a given procedure.


<a name="binaryfeatures"></a>
# **BinaryFeatures**
> BinaryFeaturesResponse BinaryFeatures (string apiKey, string fileHash)

Show the genomic features for a given binary.

### Example
```csharp
using System;
using System.Diagnostics;
using CytherealMagic.Api;
using CytherealMagic.Client;
using CytherealMagic.Model;

namespace Example
{
    public class BinaryFeaturesExample
    {
        public void main()
        {
            
            var apiInstance = new GenomicFeaturesApi();
            var apiKey = apiKey_example;  // string | API Key for accessing the service
            var fileHash = fileHash_example;  // string | A cryptographic hash of a file. Supported hashes are SHA1.

            try
            {
                // Show the genomic features for a given binary.
                BinaryFeaturesResponse result = apiInstance.BinaryFeatures(apiKey, fileHash);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling GenomicFeaturesApi.BinaryFeatures: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 

### Return type

[**BinaryFeaturesResponse**](BinaryFeaturesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="procedurefeatures"></a>
# **ProcedureFeatures**
> ProcedureFeaturesResponse ProcedureFeatures (string apiKey, string fileHash, string procRva)

Show the genomic features for a given procedure.

### Example
```csharp
using System;
using System.Diagnostics;
using CytherealMagic.Api;
using CytherealMagic.Client;
using CytherealMagic.Model;

namespace Example
{
    public class ProcedureFeaturesExample
    {
        public void main()
        {
            
            var apiInstance = new GenomicFeaturesApi();
            var apiKey = apiKey_example;  // string | API Key for accessing the service
            var fileHash = fileHash_example;  // string | A cryptographic hash of a file. Supported hashes are SHA1.
            var procRva = procRva_example;  // string | The RVA of the procedure to find similarities with. Formatted 0xnnnn

            try
            {
                // Show the genomic features for a given procedure.
                ProcedureFeaturesResponse result = apiInstance.ProcedureFeatures(apiKey, fileHash, procRva);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling GenomicFeaturesApi.ProcedureFeatures: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **procRva** | **string**| The RVA of the procedure to find similarities with. Formatted 0xnnnn | 

### Return type

[**ProcedureFeaturesResponse**](ProcedureFeaturesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

