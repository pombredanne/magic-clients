# CytherealMagic.Api.UserOperationsApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Register**](UserOperationsApi.md#register) | **GET** /register | Register to receive a MAGIC API key.


<a name="register"></a>
# **Register**
> ResponseObject Register (string email, string name)

Register to receive a MAGIC API key.

### Example
```csharp
using System;
using System.Diagnostics;
using CytherealMagic.Api;
using CytherealMagic.Client;
using CytherealMagic.Model;

namespace Example
{
    public class RegisterExample
    {
        public void main()
        {
            
            var apiInstance = new UserOperationsApi();
            var email = email_example;  // string | Email Address
            var name = name_example;  // string | Full Name

            try
            {
                // Register to receive a MAGIC API key.
                ResponseObject result = apiInstance.Register(email, name);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UserOperationsApi.Register: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **string**| Email Address | 
 **name** | **string**| Full Name | 

### Return type

[**ResponseObject**](ResponseObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

