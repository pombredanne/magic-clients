# CytherealMagic.Api.SimilarityApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**SimilarBinaries**](SimilarityApi.md#similarbinaries) | **GET** /search/binary/{api_key}/{file_hash} | Search for similar binaries.
[**SimilarProcedures**](SimilarityApi.md#similarprocedures) | **GET** /search/procs/{api_key}/{file_hash}/{proc_rva} | Search for procedures similar to a given procedure.


<a name="similarbinaries"></a>
# **SimilarBinaries**
> BinarySimilarityResponse SimilarBinaries (string apiKey, string fileHash, float? threshold, int? level)

Search for similar binaries.

### Example
```csharp
using System;
using System.Diagnostics;
using CytherealMagic.Api;
using CytherealMagic.Client;
using CytherealMagic.Model;

namespace Example
{
    public class SimilarBinariesExample
    {
        public void main()
        {
            
            var apiInstance = new SimilarityApi();
            var apiKey = apiKey_example;  // string | API Key for accessing the service
            var fileHash = fileHash_example;  // string | A cryptographic hash of a file. Supported hashes are SHA1.
            var threshold = 3.4;  // float? | Similarity threshold, default 0.7. (optional)  (default to 0.7)
            var level = 56;  // int? | Similarity Search Level from 1 through 5, default is 3. (optional)  (default to 3)

            try
            {
                // Search for similar binaries.
                BinarySimilarityResponse result = apiInstance.SimilarBinaries(apiKey, fileHash, threshold, level);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling SimilarityApi.SimilarBinaries: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **threshold** | **float?**| Similarity threshold, default 0.7. | [optional] [default to 0.7]
 **level** | **int?**| Similarity Search Level from 1 through 5, default is 3. | [optional] [default to 3]

### Return type

[**BinarySimilarityResponse**](BinarySimilarityResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="similarprocedures"></a>
# **SimilarProcedures**
> ProcedureSimilarityResponse SimilarProcedures (string apiKey, string fileHash, string procRva)

Search for procedures similar to a given procedure.

### Example
```csharp
using System;
using System.Diagnostics;
using CytherealMagic.Api;
using CytherealMagic.Client;
using CytherealMagic.Model;

namespace Example
{
    public class SimilarProceduresExample
    {
        public void main()
        {
            
            var apiInstance = new SimilarityApi();
            var apiKey = apiKey_example;  // string | API Key for accessing the service
            var fileHash = fileHash_example;  // string | A cryptographic hash of a file. Supported hashes are SHA1.
            var procRva = procRva_example;  // string | The RVA of the procedure to find similarities with. Formatted 0xnnnn

            try
            {
                // Search for procedures similar to a given procedure.
                ProcedureSimilarityResponse result = apiInstance.SimilarProcedures(apiKey, fileHash, procRva);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling SimilarityApi.SimilarProcedures: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **procRva** | **string**| The RVA of the procedure to find similarities with. Formatted 0xnnnn | 

### Return type

[**ProcedureSimilarityResponse**](ProcedureSimilarityResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

