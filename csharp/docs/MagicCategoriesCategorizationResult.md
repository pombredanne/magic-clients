# CytherealMagic.Model.MagicCategoriesCategorizationResult
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TotalCategories** | **int?** |  | [optional] 
**Categories** | [**List&lt;MagicCategoriesCategorizationResultCategories&gt;**](MagicCategoriesCategorizationResultCategories.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

