# CytherealMagic.Api.FileOperationsApi

All URIs are relative to *https://api.magic.cythereal.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Download**](FileOperationsApi.md#download) | **GET** /download/{api_key}/{file_hash} | Download a file
[**Query**](FileOperationsApi.md#query) | **GET** /query/{api_key}/{file_hash} | Query file info and analysis status
[**Reprocess**](FileOperationsApi.md#reprocess) | **GET** /reprocess/{api_key}/{file_hash} | Reprocess a previously uploaded file
[**Upload**](FileOperationsApi.md#upload) | **POST** /upload/{api_key} | Upload file for processing


<a name="download"></a>
# **Download**
> System.IO.Stream Download (string apiKey, string fileHash)

Download a file

### Example
```csharp
using System;
using System.Diagnostics;
using CytherealMagic.Api;
using CytherealMagic.Client;
using CytherealMagic.Model;

namespace Example
{
    public class DownloadExample
    {
        public void main()
        {
            
            var apiInstance = new FileOperationsApi();
            var apiKey = apiKey_example;  // string | API Key for accessing the service
            var fileHash = fileHash_example;  // string | A cryptographic hash of a file. Supported hashes are SHA1.

            try
            {
                // Download a file
                System.IO.Stream result = apiInstance.Download(apiKey, fileHash);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FileOperationsApi.Download: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 

### Return type

**System.IO.Stream**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="query"></a>
# **Query**
> QueryResponse Query (string apiKey, string fileHash)

Query file info and analysis status

### Example
```csharp
using System;
using System.Diagnostics;
using CytherealMagic.Api;
using CytherealMagic.Client;
using CytherealMagic.Model;

namespace Example
{
    public class QueryExample
    {
        public void main()
        {
            
            var apiInstance = new FileOperationsApi();
            var apiKey = apiKey_example;  // string | API Key for accessing the service
            var fileHash = fileHash_example;  // string | A cryptographic hash of a file. Supported hashes are SHA1.

            try
            {
                // Query file info and analysis status
                QueryResponse result = apiInstance.Query(apiKey, fileHash);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FileOperationsApi.Query: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 

### Return type

[**QueryResponse**](QueryResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="reprocess"></a>
# **Reprocess**
> ResponseObject Reprocess (string apiKey, string fileHash, int? priority)

Reprocess a previously uploaded file

### Example
```csharp
using System;
using System.Diagnostics;
using CytherealMagic.Api;
using CytherealMagic.Client;
using CytherealMagic.Model;

namespace Example
{
    public class ReprocessExample
    {
        public void main()
        {
            
            var apiInstance = new FileOperationsApi();
            var apiKey = apiKey_example;  // string | API Key for accessing the service
            var fileHash = fileHash_example;  // string | A cryptographic hash of a file. Supported hashes are SHA1.
            var priority = 56;  // int? | Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. (optional)  (default to 2)

            try
            {
                // Reprocess a previously uploaded file
                ResponseObject result = apiInstance.Reprocess(apiKey, fileHash, priority);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FileOperationsApi.Reprocess: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **fileHash** | **string**| A cryptographic hash of a file. Supported hashes are SHA1. | 
 **priority** | **int?**| Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. | [optional] [default to 2]

### Return type

[**ResponseObject**](ResponseObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="upload"></a>
# **Upload**
> ResponseObject Upload (string apiKey, System.IO.Stream filedata, string origFilePath, string password, int? priority, bool? force)

Upload file for processing

### Example
```csharp
using System;
using System.Diagnostics;
using CytherealMagic.Api;
using CytherealMagic.Client;
using CytherealMagic.Model;

namespace Example
{
    public class UploadExample
    {
        public void main()
        {
            
            var apiInstance = new FileOperationsApi();
            var apiKey = apiKey_example;  // string | API Key for accessing the service
            var filedata = new System.IO.Stream(); // System.IO.Stream | Binary contents of the file
            var origFilePath = origFilePath_example;  // string | Name of the file being uploaded (optional) 
            var password = password_example;  // string | If uploading a password protected zip, this field MUST contain the password. (optional) 
            var priority = 56;  // int? | Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. (optional)  (default to 2)
            var force = true;  // bool? | If true, will force existing file to be reanalyzed. (optional)  (default to false)

            try
            {
                // Upload file for processing
                ResponseObject result = apiInstance.Upload(apiKey, filedata, origFilePath, password, priority, force);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FileOperationsApi.Upload: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **apiKey** | **string**| API Key for accessing the service | 
 **filedata** | **System.IO.Stream**| Binary contents of the file | 
 **origFilePath** | **string**| Name of the file being uploaded | [optional] 
 **password** | **string**| If uploading a password protected zip, this field MUST contain the password. | [optional] 
 **priority** | **int?**| Analysis priority. Higher priority files are analyzed first. Priorities higher than default require special privileges. | [optional] [default to 2]
 **force** | **bool?**| If true, will force existing file to be reanalyzed. | [optional] [default to false]

### Return type

[**ResponseObject**](ResponseObject.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

